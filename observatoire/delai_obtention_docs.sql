-- (English version below)
-- calcul du délai moyen d'obtention d'un document pour les demandes
-- marquées comme "aboutie" par l'usager.
-- Le calcul est fait comme suit:
-- 1: lister les demandes marquées "aboutie"
-- 2: récupérer la date du dernier message reçu avant que l'usager ne
--    marque la demande comme aboutie (considéré comme la réponse à la demande)
-- 3: calculer le nombre de jours entre:
--    A: envoi initial et date de la réponse
--    B: dernier envoi et date de la réponse (par ex: renvoi suite à erreur...)
-- 4: calculer la moyenne de ces 2 valeurs sur toutes les demandes abouties
--
-- L'écart entre les 2 valeurs peut s'interpreter comme le temps "logistique"
-- pour faire arriver la demande à l'administration.

-- Calculate average time to obtaining information (considering
-- only positive replies, everything else is ignored here)
-- It works like this:
-- 1: select requests with "successful" state
-- 2: get the date of the last message received before the user
--    marked the request as successful. We consider this message as
--    the one containing the response.
-- 3: count the number of days between:
--    A: initial sending date and response date
--    B: last resend date and response date (differs from initial sending date
--       if emails are not delivered, there are clarification requests...
-- 4: average those 2 numbers over all requests
-- The delta between the 2 numbers can be seen as the "logistical time"
-- needed on average to get a request through administrative hoops.

WITH successful_requests AS (
    SELECT
        ir.id,
        ir.created_at,
        -- below is a date only, so it gets rounded to midnight.
        -- if on the same date as created_at, they can be ordered the wrong way
        ir.date_initial_request_last_sent_at
    FROM
        public.info_requests ir
    WHERE
        ir.described_state = 'successful'
),
events_per_request AS (
    SELECT
        ir.id AS ir_id,
        ir.created_at,
        -- pick the latest of the two. This might be wrong if the request was
        -- resent the same day as its initial creation.
        -- date_initial_request_last_sent_at is reset each time a followup message is sent,
        -- which can happen even after the response is received, causing some requests to
        -- look like they were answered before they were sent.
        greatest(ir.date_initial_request_last_sent_at::timestamp, ir.created_at) AS sent_at,
        -- to correct this, we look at event and recalculate the "last sent at" here
        ire_sending.created_at AS resent_at,
        ire_success.created_at  AS success_at,
        ire_response.created_at AS response_at
    FROM
        successful_requests ir
    JOIN info_request_events ire_sending
    ON ire_sending.info_request_id = ir.id
    JOIN info_request_events ire_success
    ON ire_success.info_request_id = ir.id
    JOIN info_request_events ire_response
    ON ire_response.info_request_id = ir.id
    WHERE (
        ire_sending.event_type in ('sent', 'followup_sent')
        AND (ire_success.event_type = 'status_update' AND ire_success.described_state = 'successful')
        AND (ire_response.event_type = 'response')
        AND ire_sending.created_at < ire_response.created_at
        AND ire_response.created_at < ire_success.created_at
    )
),
key_dates AS (
    SELECT
        epr.ir_id,
        min(epr.created_at) AS created_at,
        min(epr.sent_at) AS sent_at,
        max(epr.resent_at) AS resent_at,
        max(epr.success_at) as success_at_max,
        max(epr.response_at) AS response_at_max
    FROM
        events_per_request epr
    WHERE
        epr.response_at IS NOT NULL
    GROUP BY epr.ir_id
),
delays AS(
    SELECT
        kd.ir_id,
        kd.created_at,
        kd.sent_at,
        kd.resent_at,
        kd.response_at_max,
        date_part('day', kd.response_at_max - kd.created_at) AS created_delay,
        date_part('day', kd.response_at_max - kd.sent_at) AS sent_delay,
        date_part('day', kd.response_at_max - kd.resent_at) AS resent_delay
    FROM key_dates kd
)
SELECT
    count(*),
    avg(created_delay) AS avg_delay_from_creation,
    avg(sent_delay) AS avg_delay_from_last_resend,
    avg(resent_delay) AS avg_delay_from_last_sending_event
FROM delays;
