WITH valid_emails AS (
    -- compter les mails qui ne sont pas des réponses automatiques,
    -- antispam, erreurs...
    SELECT
        im.id,
        im.info_request_id,
        im.sent_at,
        im.from_email,
        tag.name,
        tag.value
    FROM
        public.incoming_messages im
        LEFT JOIN has_tag_string_tags tag ON (tag.model_id = im.id
                AND tag.model_type = 'IncomingMessage'::text)
    WHERE (tag.name IS NULL
        OR NOT (tag.value = 'accuse_reception'
            OR tag.name = 'captcha'))
    AND NOT (starts_with (lower(im.from_email), 'postmaster@')
        OR starts_with (lower(im.from_email), 'mailer-daemon')
        OR starts_with (lower(im.from_email), 'mdaemon'))
ORDER BY
    im.id ASC
),
requests_with_valid_response_within_delay AS (
    SELECT
        ir.id,
        ir.date_response_required_by,
        min(im.id),
        min(im.from_email),
        min(im.sent_at)
    FROM
        info_requests ir
        LEFT JOIN valid_emails im ON (ir.id = im.info_request_id)
    WHERE (
        -- date_initial_request_last_sent_at does not work because it is at 'midnight'
        -- as it only contains a date, not a time
        im.sent_at > (ir.created_at + '5 minutes'::interval)
        AND im.sent_at <= ir.date_response_required_by)
GROUP BY
    ir.id
),
vals AS (
    SELECT
        count(*) AS val
    FROM
        requests_with_valid_response_within_delay
),
total_reqs AS (
    -- on exclut les demandes retirées par les demandeurs et
    -- celles masquées par les admins (probablement invalides)
    SELECT
        count(*) AS tot
FROM
    info_requests
    WHERE
        prominence = 'normal'
        AND described_state != 'user_withdrawn'
)
SELECT
    1 - (vals.val::numeric / total_reqs.tot) AS taux_non_reponse
FROM
    vals,
    total_reqs;
