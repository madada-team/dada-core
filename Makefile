# shortcuts to various useful commands to setup dev envs

develop-docs:
	uvx --with 'mkdocs-material==9.5.48' --with 'mkdocs-git-revision-date-plugin==0.3.2' mkdocs serve

build-docs:
	uvx --with 'mkdocs-material==9.5.48' --with 'mkdocs-git-revision-date-plugin==0.3.2' mkdocs build

setup-ansible-deps:
	ansible-galaxy role install --roles-path ansible/roles -r ansible/requirements.yml --force
	ansible-galaxy collection install -r ansible/requirements.yml

vault_edit_prod:
	ansible-vault edit --vault-password-file .ansible_vault_pwd ansible/group_vars/production/vault.yml

vault_edit_staging:
	ansible-vault edit --vault-password-file .ansible_vault_pwd ansible/group_vars/staging/vault.yml
