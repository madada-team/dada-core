#!/bin/bash

# decide which SSH key and variables to use based on
# the current branch.
# In gitlab CI, the variables can be protected to only be
# available in protected branches, so check settings if
# the value is not accessible here
# See https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
# for details about this file

if [ "${CI_COMMIT_BRANCH}" == "master" ]; then
    echo "Running on master"
    export DEPLOY_SERVER_NAME="production"
    chmod 400 "$SSH_PRIVKEY_PRODUCTION"
    ssh-add "$SSH_PRIVKEY_PRODUCTION"
elif [ "${CI_COMMIT_BRANCH}" == "staging" ]; then
    echo "Running on staging"
    export DEPLOY_SERVER_NAME="staging"
    chmod 400 "$SSH_PRIVKEY_STAGING"
    ssh-add "$SSH_PRIVKEY_STAGING"
else
    echo "Running on a non-deploying branch"
fi
