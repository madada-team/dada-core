#!/bin/bash
set -e
# set -x

# create a file named `modified_templates.txt` in the root dir of your theme with
# the list of templates modified (from https://github.com/mysociety/alaveteli/blob/develop/doc/CHANGES.md)
# then run (from that same directory)
# path/to/find_modified_templates.sh
# it will list the names of templates that you have overridden and need to update.

cat modified_templates.txt | sed 's/^app/lib/g' | sort > modified_templates_cleaned.txt

echo "Update the following files to match the changes in alaveteli:"

find . | grep erb$ | sed 's/^.\///' | sort | comm --check-order -12 - modified_templates_cleaned.txt

echo "See changes in these files on github: https://github.com/mysociety/alaveteli/compare/0.40.1.2...0.41.0.0"
echo "(Adjust the version numbers to match yours like: OLD...NEW)"
