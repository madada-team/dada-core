-- This sql script is meant to be run from ./generate_datadump.sh
-- It will remove a variety of data from the dump in order to make it
-- ready to share publicly while maintaining anonymity of data and
-- confidentiality of alaveteli users (in particular embargoed requests)

-- Make sure the script execution stops with an error if any of the commands below
-- fail, which would indicate a possible change in database schema. We want to make
-- sure we don't silently miss those, as this could lead to confidential data being leaked.
\set ON_ERROR_STOP on

-- clean user data
DROP INDEX users_email_index;
DROP INDEX index_users_on_url_name;
DROP INDEX users_lower_email_index;

-- remove any potential personal info but keep ban notice so we can exclude
-- banned users from stats
update users set ban_text='banned' where ban_text != '';

alter table users
    drop email,
    drop name,
    drop url_name,
    drop email_bounce_message,
    drop daily_summary_hour,
    drop daily_summary_minute,
    drop info_requests_count,  -- see issue #72
    drop info_request_batches_count,
    drop login_token,
    drop about_me;
ALTER TABLE public.users
    DROP COLUMN hashed_password,
    DROP COLUMN salt,
    DROP COLUMN otp_enabled,
    DROP COLUMN otp_secret_key,
    DROP COLUMN otp_counter;

-- keep some stats about number of roles but drop raw data
alter table roles add column count integer;
update roles set count=(select count(role_id) from users_roles where role_id=roles.id);
drop table users_roles;

drop table pro_accounts;
drop table profile_photos;

-- set all authors to the same id 1

-- track_things can be sensitive as it can reveal topics people are interested
drop table track_things;
drop table track_things_sent_emails;

-- remove censor rules
-- TODO: apply these to requests content before dropping them
DROP TABLE censor_rules;

-- drop various other tables
DROP TABLE active_storage_attachments;
DROP TABLE active_storage_variant_records;
DROP TABLE active_storage_blobs;
DROP TABLE acts_as_xapian_jobs;
drop table announcement_dismissals;
drop table announcement_translations;
drop table announcements;
drop table ar_internal_metadata;
drop table dataset_values;
drop table dataset_value_sets;
drop table dataset_keys;
drop table dataset_key_sets;
drop table flipper_features;
drop table flipper_gates;
drop table incoming_message_errors;
drop table schema_migrations;
drop table webhooks;
drop table widget_votes;
drop table notifications ;

-- drop projects related tables
drop table project_submissions;
drop table project_resources;
drop table project_memberships;
drop table projects;

-- clean citations
drop index index_citations_on_user_id;
update public.citations set user_id=1;

-- clean comments
update comments set user_id=1, body='comment';

-- remove all drafts
drop table draft_info_requests;
drop table draft_info_request_batches;
drop table draft_info_request_batches_public_bodies;

-- emails and attachments
update foi_attachments set filename ='file', within_rfc822_subject='subject', hexdigest='123456';
drop table spam_addresses;
update outgoing_messages set body='msg';
alter table incoming_messages
    drop cached_attachment_text_clipped,
    drop cached_main_body_text_unfolded,
    drop cached_main_body_text_folded,
    drop from_email,
    drop subject;

drop table outgoing_message_snippet_translations;
drop table outgoing_message_snippets;

-- email logs contain a ton of personal info such as email addresses
update mail_server_logs set line='-';

alter table mail_server_logs drop mail_server_log_done_id;
drop table mail_server_log_dones;
drop table post_redirects;

-- info requests: keep as much data as possible while making it very hard/impossible to
-- correlate with the site's data
drop index index_info_requests_on_url_title;
alter table public.info_requests drop column idhash;

update public.info_requests set user_id=1;

update public.info_requests set title='hidden', url_title='hidden' where prominence='hidden';
update public.info_requests set title='requester only', url_title='requester_only' where prominence='requester_only';

update public.request_classifications set user_id=1;

drop table request_summaries;
drop table request_summaries_summary_categories;
drop table request_summary_categories;

-- keep a count of embargoed requests so we have a sense of how many we have
-- but delete them entirely to ensure confidentiality
create table if not exists public.requests_stats (embargoed_count integer);
-- make sure we only have one row of stats
delete from public.requests_stats;
insert into public.requests_stats (select count(*) from info_requests where id in (select info_request_id from public.embargoes where publish_at > now()));

-- drop user_sign_ins table although we don't actually keep any info in it
drop table user_sign_ins;

-- note that we are a bit conservative here and remove data about recently published
-- requests just in case something strange was done in the last few days.
-- This means the stats slighly underestimate numbers.
-- publish_at takes into account the values in embargo_extensions.

drop table public.user_info_request_sent_alerts;
delete from public.mail_server_logs where info_request_id in (select info_request_id from public.embargoes where publish_at > now() - interval '7 days');
delete from public.info_request_events where info_request_id in (select info_request_id from public.embargoes where publish_at > now() - interval '7 days');
delete from public.outgoing_messages where info_request_id in (select info_request_id from public.embargoes where publish_at > now() - interval '7 days');
delete from public.incoming_messages where info_request_id in (select info_request_id from public.embargoes where publish_at > now() - interval '7 days');
delete from public.comments where info_request_id in (select info_request_id from public.embargoes where publish_at > now() - interval '7 days');
delete from public.info_requests where id in (select info_request_id from public.embargoes where publish_at > now() - interval '7 days');

-- remove the embargoes records that are still hidden
delete from public.embargoes where publish_at > now() - interval '7 days';
drop table embargo_extensions;

-- batches
alter table public.info_request_batches drop body, drop title;
update public.info_request_batches set user_id=1;
drop table public.info_request_batches_public_bodies;


update public.info_request_events set params='{}';

-- public bodies
alter table public_bodies drop last_edit_editor, drop last_edit_comment, drop api_key;

alter table public_body_change_requests
    drop user_email,
    drop user_name,
    drop notes,
    drop public_body_email;
update public_body_change_requests set user_id =1;

alter table public_body_versions
    drop request_email,
    drop last_edit_comment,
    drop last_edit_editor;
alter table public_body_translations
    drop request_email;
