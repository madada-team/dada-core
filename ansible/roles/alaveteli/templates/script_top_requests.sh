#!/bin/bash
# parse nginx logs to find most visited request pages and email results
# Runs via cron:

TMPFILE=/tmp/top_requests

sudo rm -f $TMPFILE

# collect non bot log lines
sudo gunzip -c /var/log/nginx/alaveteli_ssl_access.log*.gz | rg -vi bot | rg "GET /demande/" > /$TMPFILE
sudo cat /var/log/nginx/alaveteli_ssl_access.log* | rg -vi bot | rg "GET /demande/" >> $TMPFILE

# filter lines related to requests, sort and count them, and clean up links
# so that they can be clicked in the email, and send out
cat $TMPFILE | sed 's/^.* "GET \(.*\) HTTP.*$/\1/' | sed 's/?.*//' | sort | uniq -c | sort -nr | head -n 40 | sed 's/\/demande/https:\/\/madada.fr\/demande/' | mail -r contact@madada.fr -s "Demandes les plus populaires ces 2 dernières semaines" contact@madada.fr
