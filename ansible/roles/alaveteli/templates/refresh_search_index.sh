#!/bin/bash
set -eu

# run this on a daily cron to refresh the search index in meilisearch
# which is by the observatory

export DB_USERNAME={{ db_user_name }}
export DB_PASSWORD={{ db_user_password }}
export DB_NAME={{ db_name_production }}
export MEILI_API_KEY={{ meilisearch_master_key }}
cd {{ meilisearch_home }} && {{ poetry_home }}/bin/poetry run python dada_recherche/main.py
