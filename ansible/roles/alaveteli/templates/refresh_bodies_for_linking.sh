#!/bin/bash
set -eu

# run this on a daily cron to refresh bodies_for_linker.csv
# which is then used as input to match our db with the list
# of prada scraped from cada.fr

env DB_URL="postgres://{{ db_user_name }}:{{ db_user_password }}@localhost/{{ db_name_production }}" \
bash -c 'psql -d $DB_URL -c "\copy (SELECT pbv.id, pbv.public_body_id, pbt.request_email, pbv.name, pbv.home_page, tags.name AS tag_name, nt.body AS notes FROM public_body_versions pbv JOIN public_bodies pb ON pbv.public_body_id = pb.id AND pbv.version = pb.version JOIN public_body_translations pbt ON pbt.public_body_id = pb.id JOIN has_tag_string_tags tags ON tags.model_id = pb.id AND tags.model_type='"'PublicBody'"' JOIN notes n ON n.notable_id=pb.id AND n.notable_type='"'PublicBody'"' JOIN note_translations nt ON nt.note_id=n.id WHERE tags.value IS NULL ORDER BY pbv.created_at DESC)  to '{{ alaveteli_path }}/public/restricted/bodies_for_linking.csv' with csv header"'
