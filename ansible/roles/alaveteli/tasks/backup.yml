---
#
# wal-g backup of database
#
- name: Install wal-g from github releases
  ansible.builtin.unarchive:
    src: https://github.com/wal-g/wal-g/releases/download/v{{ wal_g_version }}/wal-g-pg-ubuntu-20.04-amd64.tar.gz
    dest: /usr/local/bin/
    mode: "0554"
    owner: root
    group: postgres
    remote_src: true

- name: Setup cronjob to run a base backup once per day
  become: true
  become_user: postgres
  ansible.builtin.cron:
    name: "daily WAL-g base backup"
    job: "/usr/bin/envdir {{ db_backup_walg_envdir }} /usr/local/bin/wal-g backup-push /var/lib/postgresql/13/main"
    minute: "7"
    hour: "1"
    user: postgres
  when: inventory_hostname == 'madada.fr'

- name: Setup cronjob to prune old backups once per day
  become: true
  become_user: postgres
  ansible.builtin.cron:
    name: "Remove old WAL-g base backups"
    job: "/usr/bin/envdir {{ db_backup_walg_envdir }} /usr/local/bin/wal-g delete retain FULL 90 --confirm"
    minute: "37"
    hour: "2"
    user: postgres
  when: inventory_hostname == 'madada.fr'

- name: Setup envdir directory
  ansible.builtin.file:
    path: "{{ db_backup_walg_envdir }}"
    state: directory
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | path
  ansible.builtin.copy:
    content: "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    dest: "{{ db_backup_walg_envdir }}/PATH"
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | pghost
  ansible.builtin.copy:
    content: /var/run/postgresql/
    dest: "{{ db_backup_walg_envdir }}/PGHOST"
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | endpoint
  ansible.builtin.copy:
    content: "{{ db_backup_storage_endpoint }}"
    dest: "{{ db_backup_walg_envdir }}/AWS_ENDPOINT"
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | access key id
  ansible.builtin.copy:
    content: "{{ backup_storage_access_key }}"
    dest: "{{ db_backup_walg_envdir }}/AWS_ACCESS_KEY_ID"
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | secret access key
  ansible.builtin.copy:
    content: "{{ backup_storage_secret_key }}"
    dest: "{{ db_backup_walg_envdir }}/AWS_SECRET_ACCESS_KEY"
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | S3 prefix
  ansible.builtin.copy:
    content: "{{ db_backup_storage_prefix }}"
    dest: "{{ db_backup_walg_envdir }}/WALG_S3_PREFIX"
    owner: root
    group: postgres
    mode: "0750"

- name: Setup backup env | compression method
  ansible.builtin.copy:
    content: brotli
    dest: "{{ db_backup_walg_envdir }}/WALG_COMPRESSION_METHOD"
    owner: root
    group: postgres
    mode: "0750"

#
# RESTIC backup of files (incoming emails)
#

- name: Download restic official binary from github release page
  ansible.builtin.get_url:
    url: https://github.com/restic/restic/releases/download/v{{ restic_version }}/restic_{{ restic_version }}_linux_amd64.bz2
    dest: /tmp
    mode: "0644"

- name: Extract restic
  ansible.builtin.shell: bzip2 -cd /tmp/restic_{{ restic_version }}_linux_amd64.bz2 > /usr/local/bin/restic
  changed_when: true

- name: Set restic owner and permissions
  ansible.builtin.file:
    path: /usr/local/bin/restic
    owner: root
    group: root
    mode: "0754"

- name: Setup restic envdir directory
  ansible.builtin.file:
    path: "{{ files_backup_restic_envdir }}"
    state: directory
    owner: root
    group: root
    mode: "0750"

- name: Setup restic backup env | restic repository
  ansible.builtin.copy:
    content: "{{ files_backup_restic_repository }}"
    dest: "{{ files_backup_restic_envdir }}/RESTIC_REPOSITORY"
    owner: root
    group: root
    mode: "0750"

- name: Setup restic backup env | restic password
  ansible.builtin.copy:
    content: "{{ files_backup_restic_password }}"
    dest: "{{ files_backup_restic_envdir }}/RESTIC_PASSWORD"
    owner: root
    group: root
    mode: "0750"

- name: Setup restic backup env | access key
  ansible.builtin.copy:
    content: "{{ backup_storage_access_key }}"
    dest: "{{ files_backup_restic_envdir }}/AWS_ACCESS_KEY_ID"
    owner: root
    group: root
    mode: "0750"

- name: Setup restic backup env | secret key
  ansible.builtin.copy:
    content: "{{ backup_storage_secret_key }}"
    dest: "{{ files_backup_restic_envdir }}/AWS_SECRET_ACCESS_KEY"
    owner: root
    group: root
    mode: "0750"

- name: Setup restic backup cron job for ActiveStorage files
  ansible.builtin.cron:
    name: "Hourly restic backup (ActiveStorage)"
    job: "/usr/bin/envdir {{ files_backup_restic_envdir }} /usr/local/bin/restic backup {{ alaveteli_path }}/storage/"
    minute: "44"
    hour: "*"
    user: root
  when: inventory_hostname == 'madada.fr'

- name: Setup restic pruning cron job
  # this removes snapshots older than 3 months, to be in line with our privacy policy
  ansible.builtin.cron:
    name: "Daily pruning of restic backups"
    job: "/usr/bin/envdir {{ files_backup_restic_envdir }} /usr/local/bin/restic forget --keep-within 90d --prune"
    minute: "49"
    hour: "3"
    user: root
  when: inventory_hostname == 'madada.fr'
