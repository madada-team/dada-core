---
# the alaveteli repo to use
# Note: this is the alaveteli app which is normally fetched from
# https://github.com/mysociety/alaveteli/
# but this setting allows us to use a fork and branch
# for hotfixes or customisations
alaveteli_repo: "https://github.com/mysociety/alaveteli/"
# the branch, tag or commit ref to checkout. Defaults to master
alaveteli_commit: "0.44.0.0"
ruby_version: "3.2.2"

# keep emails locally delivered on staging server as we seem to have
# lots of smtp connection issues
# FIXME: change this back to an external address once smpt is fixed
webmaster_email: "{{ vault_webmaster_email }}"
postmaster_email: "{{ vault_postmaster_email }}"
dpo_email: "{{ webmaster_email }}"

# the list of email addresses to forward messages sent to contact@madada
contact_madada_forward_emails: "{{ vault_contact_madada_forward_emails }}"

# email address used to post requests on behalf of madada
bourriquet_forward_emails: "{{ vault_bourriquet_forward_emails }}"

db_name_production: alaveteli_production
db_name_test: alaveteli_test
db_user_password: "{{ vault_db_user_password }}"

# this one is in vault.yml, encrypted
rails_secret_key_base: "{{ vault_rails_secret_key_base }}"

site_domain: dadastaging.okfn.fr

# hostname of the server
server_name: dadastage
server_fqdn: "{{ server_name }}.{{ site_domain }}"

incoming_email_secret: "{{ vault_incoming_email_secret }}"

emergency_admin_password: "{{ vault_emergency_admin_password }}"

# the recaptcha keys come from https://www.google.com/recaptcha/admin/site/345908267/settings
# the site key can be public, as it appears in the HTML page sent to user browsers
recaptcha_site_key: 6LcrJJ4UAAAAAAokc-1h06cfzlQgTsjeQMsyd7en
recaptcha_secret_key: "{{ vault_recaptcha_secret_key }}"

# send all requests to public bodies to this email instead
# useful for staging, set to empty string in production
override_all_public_body_emails: "{{ webmaster_email }}"

# change the prefix for staging/production
# to limit confusion
incoming_email_short_prefix: dadastaging
incoming_email_prefix: "{{ incoming_email_short_prefix }}+"

# git branch in theme repo to use for the site
# the production site should use master
theme_branch: staging

# folder where exim will write incoming emails
raw_emails_location: files/raw_emails

# api token to download the translations
# Any transifex account that has access to them should work
transifex_token: "{{ vault_transifex_token }}"

# disable db backup on staging server by not specifying any values here
backup_storage_access_key: ""
backup_storage_secret_key: ""

db_backup_walg_envdir: /etc/wal-g.d/env
db_backup_archive_mode: "off"
db_backup_archive_command: ""
db_backup_storage_endpoint: "https://sos-ch-gva-2.exo.io"
db_backup_storage_prefix: "s3://madada-db-backup"

# files backup, for incoming emails
files_backup_restic_envdir: /etc/restic/env
files_backup_restic_repository: "{{ vault_files_backup_restic_repository }}"
files_backup_restic_password: "{{ vault_files_backup_restic_password }}"

maxmind_license_key: "{{ vault_maxmind_license_key }}"

opendkim_private_key: "{{ vault_opendkim_private_key }}"

# metabase, stats.madada.fr
stats_site_url: invalidurl.dadastaging.okfn.fr
metabase_db_name: metabase
metabase_db_user: metabase
metabase_db_password: "{{ vault_metabase_db_password }}"

# meilisearch
search_site_url: recherche.dadastaging.okfn.fr

docs_site_url: docs.dadastaging.okfn.fr
