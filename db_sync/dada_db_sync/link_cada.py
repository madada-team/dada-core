import json
import os
import re
import warnings
from base64 import b64encode

import pandas as pd
import splink.duckdb.comparison_library as cl
import splink.duckdb.comparison_template_library as ctl
from splink.datasets import splink_datasets
from splink.duckdb.linker import DuckDBLinker

warnings.filterwarnings("ignore")

path_prefix = "annuaire_cada"

prada_dir_filename = f"{path_prefix}/annuaire_prada_web.csv"
madada_dir_filename = "https://madada.fr/restricted/bodies_for_linking.csv"
linking_results_full_filename = f"{path_prefix}/linking_matrix.csv"
linking_matrix_filename = f"{path_prefix}/linking_matrix.csv"
cada_directory_dict_filename = f"{path_prefix}/cada_directory_dict.json"
prada_diff_filename = f"{path_prefix}/diff.csv"


def clean_madada_df(df_madada: pd.DataFrame) -> pd.DataFrame:
    # df_madada uses the public_body_id for this
    df_madada.rename(
        columns={
            "public_body_id": "unique_id",
            "name": "nom",
            "tag_name": "categorie",
        },
        inplace=True,
    )
    df_madada.notes = df_madada.notes.str.replace("\n", ",")
    df_madadacp = df_madada.notes.str.extract(
        r"(?:, ?(\d{5})[, /]|, (\d{4}), [^\d])", expand=False
    )
    df_madada["cp"] = (
        df_madadacp.bfill(axis=1).iloc[:, 0].str.pad(5, side="left", fillchar="0")
    )
    # df_madada["departement"] = df_madada["cp"].str.substring(2)
    df_madada["ville"] = df_madada.notes.str.extract(r", ?\d{4,5}[, /]([^,|]*)")
    df_madada.ville = df_madada.ville.str.replace(
        r" Cedex[-| \d\r]*$", "", flags=re.IGNORECASE, regex=True
    )
    df_madada.ville = df_madada.ville.str.lstrip(" /").str.lower()

    # remove useless columns
    df_madada.drop(["id", "home_page", "request_email", "notes"], axis=1, inplace=True)
    # remove public bodies that we don't use

    df_madada.nom = df_madada.nom.str.replace(r"[,-/']", "", regex=True)
    df_madada.nom = df_madada.nom.str.replace(
        r" de la | de l'| du | de | des ", " ", regex=True
    )
    df_madada.nom = df_madada.nom.str.lower().replace(r" {1,99}", " ", regex=True)
    df_madada.nom = df_madada.nom.str.replace("œ", "oe")
    df_madada.nom = (
        df_madada.nom.str.normalize("NFKD")
        .str.encode("ascii", errors="ignore")
        .str.decode("utf-8")
    )
    df_madada.ville = (
        df_madada.ville.str.normalize("NFKD")
        .str.encode("ascii", errors="ignore")
        .str.decode("utf-8")
    )

    # we have several tags that map to the CADA's "Etablissement public territorial"
    df_madada.categorie = df_madada.categorie.str.replace("epci", "ept")
    df_madada.categorie = df_madada.categorie.str.replace("chu", "ept")
    df_madada.categorie = df_madada.categorie.str.replace("epci", "ept")
    df_madada.categorie = df_madada.categorie.str.replace("prefecture", "services_etat")
    return df_madada


def clean_cada_df(df_cada: pd.DataFrame) -> pd.DataFrame:
    ## clean data so that values match

    # use the url as a unique id for the CADA directory as they don't have a real one
    df_cada["unique_id"] = df_cada["url_id"]  # range(len(df_cada))
    df_cada.rename(
        columns={
            "Classement de l'administration": "categorie",
            "Département de l'autorité": "departement",
            "Nom de l'administration": "nom",
            "Code postal/Ville de l'autorité": "cp_ville",
        },
        inplace=True,
    )

    # categories
    df_cada.categorie = df_cada.categorie.str.replace("Mairie", "mairie")
    df_cada.categorie = df_cada.categorie.str.replace(
        "Établissement public territorial", "ept"
    )
    df_cada.categorie = df_cada.categorie.str.replace(
        "Services de l'État", "services_etat"
    )
    df_cada.categorie = df_cada.categorie.str.replace("Conseil régional", "cr")
    df_cada.categorie = df_cada.categorie.str.replace("Conseil départemental", "cg")
    # si le nom commence par "Préfecture" (et que le nom de contient pas "police") alors ça en est une!
    # ce qui évitera de nombreuses erreurs avec d'autres administrations basées dans les mêmes villes
    # idem rectorat
    df_cada.loc[df_cada.nom.str.startswith("Rectorat"), "categorie"] = "rectorat"
    df_cada.loc[df_cada.nom.str.startswith("Préfecture"), "categorie"] = "prefecture"
    df_cada.loc[df_cada.nom.str.startswith("Université"), "categorie"] = "universite"
    df_cada.loc[
        df_cada.nom.str.startswith("Communauté d'agglomération"), "categorie"
    ] = "communaute_agglos"
    df_cada.loc[
        df_cada.nom.str.startswith("Communauté de commune"), "categorie"
    ] = "communaute_communes"
    df_cada.loc[
        df_cada.nom.str.startswith("Centre hospitalier"), "categorie"
    ] = "hopital"

    df_cpv = df_cada.adresse.str.extract(
        r",(?P<cp>\d{5}) (?P<ville>[^,]*?)(?: CEDEX.*)?(?: \d{1,2})?$", expand=True
    )
    df_cada["cp"] = df_cpv["cp"]
    df_cada["ville"] = df_cpv["ville"].str.lower()
    del df_cpv

    df_cada.nom = df_cada.nom.str.replace("Mairie des ", "Mairie - Les ")
    df_cada.nom = df_cada.nom.str.replace("Mairie de ", "Mairie - ")
    df_cada.nom = df_cada.nom.str.replace("Mairie d'", "Mairie - ")
    df_cada.nom = df_cada.nom.str.replace("Mairie du ", "Mairie - Le ")
    df_cada.nom = df_cada.nom.str.replace("œ", "oe")
    df_cada.nom = df_cada.nom.str.replace(r"[,-/']", "", regex=True)
    df_cada.nom = df_cada.nom.str.replace(
        r" de la | de l'| du | de | des ", " ", regex=True
    )
    df_cada.nom = (
        df_cada.nom.str.normalize("NFKD")
        .str.encode("ascii", errors="ignore")
        .str.decode("utf-8")
    )
    # remove multiples spaces
    df_cada.nom = df_cada.nom.str.lower().replace(r" {1,99}", " ", regex=True)
    df_cada.ville = (
        df_cada.ville.str.normalize("NFKD")
        .str.encode("ascii", errors="ignore")
        .str.decode("utf-8")
    )
    return df_cada


def load_and_clean_dfs() -> tuple[pd.DataFrame, pd.DataFrame]:
    user = os.getenv("HTTP_RESTRICTED_USER", "username")
    password = os.getenv("HTTP_RESTRICTED_PASS", "password")

    storage_options = {
        "Authorization": b"Basic %s" % b64encode(f"{user}:{password}".encode())
    }
    df_cada = clean_cada_df(pd.read_csv(prada_dir_filename))
    df_madada = clean_madada_df(
        pd.read_csv(madada_dir_filename, storage_options=storage_options)
    )
    return df_madada, df_cada


def get_blocking_rules() -> list[str]:
    blocking_rule_0 = "l.categorie = r.categorie"
    blocking_rule_1 = "left(l.cp, 3) = left(r.cp, 3)"
    blocking_rule_2 = f"{blocking_rule_0} AND {blocking_rule_1}"
    return [blocking_rule_2]


def train_linker(df_madada: pd.DataFrame, df_cada: pd.DataFrame) -> DuckDBLinker:
    df_l = df_madada
    df_r = df_cada

    comparison_cp = {
        "output_column_name": "cp",
        "comparison_description": "Compare post codes",
        "comparison_levels": [
            {
                "sql_condition": "cp_l IS NULL OR cp_r IS NULL",
                "label_for_charts": "Null",
                "is_null_level": True,
            },
            {
                "sql_condition": "cp_l = cp_r",
                "label_for_charts": "Exact match",
            },
            {
                "sql_condition": "left(cp_l, 4) = left(cp_r, 4)",
                "label_for_charts": "Last digits differ",
            },
            {
                "sql_condition": "left(cp_l, 3) = left(cp_r, 3)",
                "label_for_charts": "Last digits differ",
            },
            {"sql_condition": "ELSE", "label_for_charts": "All other comparisons"},
        ],
    }

    settings = {
        "link_type": "link_only",
        "blocking_rules_to_generate_predictions": get_blocking_rules(),
        "comparisons": [
            ctl.name_comparison("nom"),
            comparison_cp,
            ctl.name_comparison("ville"),
        ],
        "retain_intermediate_calculation_columns": True,
        "retain_matching_columns": True,
    }

    linker = DuckDBLinker(
        [df_l, df_r], settings, input_table_aliases=["df_left", "df_right"]
    )

    linker.probability_two_random_records_match = 1 / 50_000

    linker.estimate_u_using_random_sampling(max_pairs=1e5, seed=1)

    training_blocking_rule = get_blocking_rules()
    linker.estimate_parameters_using_expectation_maximisation(training_blocking_rule[0])

    training_blocking_rule2 = "l.ville = r.ville"
    linker.estimate_parameters_using_expectation_maximisation(training_blocking_rule2)
    return linker


def predict_results(linker: DuckDBLinker, threshold: float = 0.6) -> pd.DataFrame:
    results = linker.predict(threshold_match_probability=threshold)
    results_df = results.as_pandas_dataframe().sort_values(["match_probability"])
    return results_df


def save_linker_results(results_df: pd.DataFrame) -> pd.DataFrame:
    """Save results in different formats and return the linking matrix as df"""
    results_df.to_csv(linking_results_full_filename, index=False)
    results_df.rename(
        columns={"unique_id_l": "id_madada", "unique_id_r": "url_cada"}, inplace=True
    )
    # we still potentially have duplicates in the results here
    results_df.sort_values(["match_probability"], ascending=False, inplace=True)
    linking_matrix = results_df[["id_madada", "url_cada"]].copy()
    linking_matrix.to_csv(linking_matrix_filename, index=False)
    return linking_matrix


def build_json_cada_directory_for_alaveteli(linking_matrix: pd.DataFrame) -> dict:
    """Output a dict like:
    {"<madada_id>": {
        "nom": "name of public body",
        "prada": "name of prada",
        "email": "email address",
        "adresse": "postal address",
        },
     "<id_#2>":...
     }
    """
    df_cada_original = pd.read_csv(prada_dir_filename)
    joined_df = df_cada_original.merge(
        linking_matrix, how="inner", left_on="url_id", right_on="url_cada"
    )
    joined_df.drop(["url_id"], axis=1, inplace=True)
    joined_df.drop_duplicates("id_madada", keep="first", inplace=True)
    joined_df.set_index("id_madada", inplace=True)
    # some emails are missing, and come out as NaN
    joined_df.email.fillna("", inplace=True)
    d = joined_df.to_dict(orient="index")
    return d


def load_and_link_prada_directory():
    linker = train_linker(*load_and_clean_dfs())
    results_df = predict_results(linker, 0.6)
    linking_matrix = save_linker_results(results_df)
    d = build_json_cada_directory_for_alaveteli(linking_matrix)
    with open(cada_directory_dict_filename, "w") as dict_file:
        json.dump(d, dict_file, ensure_ascii=False)

    # add madada_id to the diff.csv file
    diff_df = pd.read_csv(prada_diff_filename)
    diff_df = diff_df.merge(
        linking_matrix, how="left", left_on="url_id", right_on="url_cada"
    )
    diff_df.drop(["url_id"], axis=1, inplace=True)
    diff_df.to_csv(prada_diff_filename, index=False)


if __name__ == "__main__":
    load_and_link_prada_directory()
