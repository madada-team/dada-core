# Configuration et déploiement de Ma Dada

Ce repo contient les fichiers de configuration et le code `ansible` pour déployer le site de DADA.

Lire le [README du thème](dada-france-theme/README.md) pour quelques explications sur quoi modifier où.

Il y'a un paquet de variables et valeurs de config qui devraient pour la plupart se trouver
dans le dossier `ansible/group_vars/`. Voir `ansible/group_vars/all/main.yml` pour une brève
explication sur l'organisation des variables.

Les variables sensibles sont encryptées dans les fichiers `vault.yml` sous `group_vars`. Le `Makefile` contient quelques
commandes utiles. Le mot de passe ansible vault peut être enregistré avec:

```bash
echo <password> > .ansible_vault_pwd
```

pour permettre l'édition de ces fichiers encryptés.

## Déploiement - Comment ça marche

A chaque `git push` sur le repo, l'intégration continue de gitlab déploie une mise à jour du site via [ansible](https://docs.ansible.com).

Il y'a 2 branches principales, qui correspondent chacune à un serveur:

- `master` déploie sur le serveur principal https://madada.fr
- `staging` déploie sur le serveur de staging/dev https://dadastaging.okfn.fr.

Les "features branches" ne sont pas pour l'instant déployées automatiquement.

## Contribuer

### Documentation

#### Installation locale

Ces instructions marchent sous linux, probablement Mac. Non testé sous windows.

```bash
# depuis le dossier qui contient ce fichier:
python -m venv venv_doc
. venv_doc/bin/activate  # ne pas oublier le point en début de ligne
python -m pip install -r docs/requirements.txt

# pour voir la doc mise à jour en temps réel pendant l'écriture
# et ouvrir http://localhost:8000/ dans un navigateur web
# Cette page se remet à jour à chaque enregistrement d'un fichier doc
mkdocs serve

# mettre à jour les docs dans le dossier ./docs
# Une fois les mises à jour terminées:
# créer une nouvelle branche git à partir de doc-public
# git commit les changements sur cette branche
# ouvrir une Merge Request avec `doc-public` comme branche de base
```

#### Editer directement

En haut de chaque page, le site affiche une icône "stylo" qu'il suffit de cliquer pour ouvrir le code dans gitlab. Il
n'y a rien à installer, il faut juste un compte gitlab.com.
