<style>
div.md-content {
  text-align: justify;
}
nav.md-tabs {
  display:none;
}
</style>

# Guide pratique du droit d'accès

## Le droit d’accès aux documents administratifs

Le droit d’accès aux documents administratifs, qui a valeur constitutionnelle, découle de l’article 15 de la Déclaration des droits de l’homme et du citoyen de 1789 :

> La société a le droit de demander compte à tout agent public de son administration.

Depuis la « loi CADA » de 1978, toute personne peut ainsi obtenir de nombreux documents publics : contrats, factures, statistiques, autorisations d’urbanisme, notes de frais, rapports d’expertise, code informatique, etc.

Les administrations au sens large (ministères, mairies, hôpitaux, écoles, autorités indépendantes, entreprises chargées d’une mission de service public, etc.) sont en effet tenues de transmettre aux citoyens, sur demande et sans justification particulière, les documents publics qu’ils sollicitent.

Tous ces documents, dits administratifs, sont « communicables » de droit, par principe, **à moins qu’ils ne soient couverts par un secret protégé par la loi** : secret défense, respect de la vie privée, secrets des affaires, etc. Dans ces cas, soit le document ne peut être divulgué, soit l’administration doit procéder à l’occultation de certains passages.

En cas de refus de la part de l’administration, il est possible de saisir la Commission d’accès aux documents administratifs (CADA), qui fait alors office de médiateur, préalablement à un éventuel recours devant le juge administratif.

## Quels documents peut-on obtenir ?

<a name="liste-docs"></a>

!!! abstract "DÉCISIONS PUBLIQUES"

    ![Icone Décisions publiques](media/guide-icone-decisions.png){ align=right }

    - Délibérations (de conseil municipal…)
    - Autorisations
    - Dossiers (d’enquête publique, etc.)
    - Etudes d’impact
    - Avis
    - Rapports (d’expertise, d’inspection…)
    - Correspondances (mails, courriers, etc..)
    - Comptes rendus

!!! marches-publics "MARCHÉS PUBLICS"

    ![Icone Marchés publics](media/guide-icone-marches-publics.png){ align=right }

    - Avis d’appel public à la concurrence
    - Cahiers des charges (CCAP et CCTP)
    - Liste des candidats
    - Procès-verbal d’ouverture des plis
    - Méthode de notation utilisée
    - Offres de prix globales
    - Lettre de notification du marché
    - Dossier de candidature
    - Bons de commande
    - Factures
    - Calendrier d’exécution
    - Avenants
    - Procès-verbal de réception

!!! donnees-stats "DONNÉES & STATISTIQUES"

    ![Icone Données & statistiques](media/guide-icone-stats.png){ align=right }

    - Statistiques (nombre de demandes d’asile, etc.)
    - Listes
    - Tableaux (des effectifs...)

!!! budget "BUDGETS & COMPTES PUBLICS"

    ![Icone Budgets & comptes publics](media/guide-icone-budget.png){ align=right }

    - Budget (d’une mairie, etc.)
    - Comptes administratifs
    - Pièces justificatives des dépenses
    - Notes de frais
    - Tableaux d’amortissement des emprunts
    - Comptes de gestion
    - Livres comptables
    - Titres de recettes et de dépenses
    - Fiches de paie

!!! subventions "SUBVENTIONS"

    ![Icone subventions ](media/guide-icone-subventions.png){ align=right }

    - Dossiers de demandes de subvention
    - Décisions d’attribution
    - Tableau des subventions accordées
    - Convention de subvention
    - Compte rendu financier de subvention
    - Rapports de commissaires aux comptes

!!! enviro "ENVIRONNEMENT"

    ![Icone environnement](media/guide-icone-environnement.png){ align=right }

    - Résultats (de mesures de bruit, d’analyse de
    l’eau ou de l’air...)
    - Rapports d’expertise (abattage d’arbres...)
    - Autorisations de mise sur le marché
    - Études d’impact (de nuisance sonores, etc.)
    - Demandes de dérogation “espèces protégées”
    - Demandes d’autorisations (éoliennes, IOTA, IPCE...)

!!! abstract "CONTRATS"

    ![Icone contrats](media/guide-icone-contrats.png){ align=right }

    - Conventions (de financement, d’acquisition, etc.)
    - Contrats de partenariat
    - Compromis/promesse de vente
    - Contrats de travail des agents publics
    - Baux
    - Contrats d’objectifs et de moyens

!!! lobbying "LOBBYING"

    ![Icone lobbying](media/guide-icone-lobbying.png){ align=right }

    - Correspondances émises ou reçues (par
      exemple mails ou courriers échangés avec des
      représentants d’intérêts)
    - Comptes rendus de réunions
    - Feuilles de présence
    - Liste des rendez-vous d’un élu ou d’un agent
      public

!!! urbanisme "URBANISME"

    ![Icone urbanisme](media/guide-icone-urbanisme.png){ align=right }

    - Dossiers de permis de construire
    - Avis techniques (ABF, géomètres...)
    - Plan local d’urbanisme (PLU)
    - Plan d’occupation des sols
    - Carte des voies communales
    - Rapports de commission de sécurité incendie
    - Dossiers techniques amiante de bâtiments
      publics

!!! info "DIVERS"

    ![Icone divers](media/guide-icone-divers.png){ align=right }

    - Photos et vidéos
    - Codes sources et algorithmes
    - Modèles d’apprentissage d’IA
    - Numérisations d'œuvres
    - Menus

De nombreux autres documents peuvent être communicables.
En cas de doute, n’hésitez pas à [contacter Ma Dada](mailto:contact@madada.fr)!

## Focus sur les informations relatives à l’environnement

Vous souhaitez obtenir des informations à caractère environnemental ? Bonne nouvelle : la législation prévoit **un accès facilité à ces informations**. D’une part, **vous pouvez demander directement une information**, et non un document. D’autre part, certains secrets faisant normalement obstacle à la divulgation de ces informations (secret des affaires notamment) peuvent ne pas s’appliquer.

!!! world "Qu’est-ce qu’une information à caractère environnemental ?"

    Cette notion est très extensive. Elle concerne notamment :

    - **L’état de l’environnement** à proprement parler : comme l’eau, l’air, le sol, la biodiversité, les paysages, l’atmosphère, les zones marines, etc.
    - **Les activités et facteurs susceptibles d'avoir des incidences** sur l'état de l’environnement : pollution (sonore, olfactive, de l’air, des sols...), déchets, rayonnements, etc.
    - **La santé humaine**, dès lors que celle-ci peut être altérée par des éléments évoqués aux deux points précédents : pollution de bâtiments, risques d’inondations, etc.

    #### ET CONCRÈTEMENT ?

    La CADA a ainsi regardé comme comportant des informations relatives à l’environnement :

    - des cartes de contamination des sols
    - des études d’impact de nuisances sonores
    - des comptes rendus relatifs à la délivrance de capacités de pêche
    - des inventaires de zones humides
    - une base de données climatographiques
    - les plans du réseau d’évacuation des eaux d’une station d’épuration
    - le diagnostic technique amiante de bâtiments publics
    - un plan de gestion des bois et forêts

### Quel impact sur votre demande ?

Les motifs de refus sont bien plus restreints pour l’administration. Il n’est par exemple pas toujours nécessaire que le document soit définitif. Le secret des affaires ou même le respect de la vie privée des personnes peut également ne pas s’appliquer.

_« Dans tous les cas, l’administration ne peut opposer un refus de communication qu’après avoir apprécié l’« intérêt » que celle-ci présenterait, notamment pour la protection de l’environnement et les intérêts que défend le demandeur »_, explique par ailleurs la CADA.

N’hésitez donc pas à souligner cela dans votre demande !

## Comment le droit d’accès peut-il m’aider ?

Le droit d'accès aux documents administratifs peut être utile dans différents cas de
figure, et notamment pour :

!!! note "COLLECTE DE DONNÉES ET RECHERCHE D’INFORMATIONS"

    Accédez à des délibérations, des statistiques, des avis d’experts, des factures, des échanges de mails, etc. Pour vous aider à comprendre pourquoi des décisions publiques ont été prises et qui en est responsable.

    > Un utilisateur de Ma Dada, qui s’interrogeait quant au coût de la création d’un potager municipal à Strasbourg, [a pu obtenir](https://madada.fr/demande/cout_du_potager_sur_la_place_de) le détail des fournitures (végétaux, matériel...) ainsi que du temps de travail des agents municipaux.

!!! note "CONTRÔLE DE L’ACTION PUBLIQUE"

    Vérifiez dans quelles conditions les décisions publiques sont prises, puis appliquées. Cela peut par exemple vous donner la possibilité de contester la légalité comme la pertinence de décisions.

    > Un utilisateur de Ma Dada a [obtenu le bulletin de salaire d’Emmanuel Macron](https://madada.fr/demande/bulletins_de_salaire_du_presiden#incoming-9095), ce qui lui a permis de vérifier dans quelle mesure la rémunération du chef de l’État était conforme aux textes applicables.

!!! note "PARTICIPATION À LA VIE DÉMOCRATIQUE"

    Impliquez-vous davantage dans les débats publics et **alimentez la vie démocratique** grâce aux documents administratifs (environnement, dépenses publiques, impacts sanitaires...).

    > Une personne se demande ce qui a conduit sa commune à abandonner un projet de piste cyclable. Elle peut demander à la mairie les délibérations de conseil municipal afférentes à cette décision, de même que les documents liés : comptes rendus de réunions, rapports d’expertise technique et/ou financière, les correspondances éventuellement échangées avec des représentants d’intérêts, etc.

!!! note "DÉFENSE DE SES DROITS"

    Si vous estimez que vos droits n’ont pas été respectés ou que des décisions administratives vous ont porté préjudice, l'accès aux documents administratifs peut vous aider à **obtenir des preuves pour d’éventuels recours**.

    > Une personne se demande si son voisin a bien été autorisé à installer sa piscine si près de sa maison. Elle peut obtenir le dossier de permis de construire ou de déclaration préalable des travaux, avec de nombreuses pièces annexes (plans, notices descriptives, etc.), ainsi que les éventuels avis de tiers (architectes des bâtiments de France, services instructeurs, etc.).

## Réussir sa demande d’accès

Effectuer une demande d’accès à des documents administratifs est à la portée du plus grand nombre : un mail suffit dans la plupart des cas. Voici les deux étapes-clés d’une demande !

#### 1 - Identifier les documents à solliciter

Le droit insiste bien sur le fait que le droit d’accès porte sur des documents, non des informations. Bien entendu, vous trouverez des informations publiques au sein de ces documents.

!!! tip "NOTRE CONSEIL"

    Reliser la [liste des documents](#liste-docs) plus haut, afin d’être le plus précis possible dans l’identification des documents à demander ! Attention néanmoins, il faut bien garder en tête que pour être « communicable », un document doit absolument être définitif : on ne peut pas obtenir de brouillon ou de document visant à préparer une décision future.

#### 2 - Saisir la bonne administration

L’idéal est d’envoyer sa demande à l’administration ayant produit le document, mais vous pouvez tout aussi bien saisir n’importe quelle administration qui détiendrait également ce document (voire les deux !). Vous pouvez envoyer votre demande par tout moyen laissant une trace écrite (mail, courrier...).

Nous vous invitons néanmoins à utiliser notre plateforme associative [Ma Dada](https://madada.fr) !

Forte d’une base regroupant les coordonnées de plus de 50 000 administrations (mairies, ministères, universités, etc.), Ma Dada vous accompagne tout au long de votre requête, grâce à des fonctionnalités pratiques et gratuites. Fini le parcours du combattant ! La plateforme est à vos côtés à chaque étape pour maximiser vos chances de succès.

## Pourquoi utiliser ma dada ?

**Des centaines de documents publics** : Accédez aux nombreux documents obtenus par d’autres utilisateurs, ou sollicitez-en si vous ne trouvez pas ceux que vous cherchez !

**Une aide à la rédaction** : La plateforme vous guide dans la rédaction de votre demande, et apporte de nombreux conseils utiles pour que celle- ci aboutisse.

**Faire face à la bureaucratie** : Ma Dada vous accompagne du début à la fin de votre demande d’accès, avec notamment des rappels automatiques et la possibilité de saisir la CADA directement depuis notre plateforme, très simplement.

**Un carnet d’adresses bien rempli** : la plateforme repose sur une base de données de plus de 50 000 autorités publiques, régulièrement mises à jour. Pas besoin de chercher le bon interlocuteur, on le fait pour vous !

**Alimentez le débat public** : Les demandes et réponses sont automatiquement publiées en ligne, sur Ma Dada, afin de responsabiliser les autorités, mais aussi de rendre publiques des informations... publiques !

**Des outils spécifiques pour les associations ou journalistes** : envoi d’une même demande à plusieurs administrations, embargo... Ma Dada s’adapte aux besoins de ses utilisateurs.

## Comment faire une demande sur ma dada ?

Ma Dada, c’est la possibilité d’envoyer une demande d’accès gratuitement et facilement, en quelques minutes !

### Première étape : Créer un compte ou se connecter à Ma Dada

![Capture d'écran: création de compte ou connexion](media/madada-screenshot-login.png){ align=left }

### Deuxième étape : Cliquer sur “Faire une demande”

La plateforme vous propose de sélectionner l’administration à laquelle vous souhaitez envoyer votre demande, en tapant son nom (mairie de Paris, ministère du Travail, etc.).

<figure markdown="span">
![Capture d'écran: nouvelle demande](media/madada-screenshot-nouvelle-demande.png)
</figure>

Si vous ne trouvez pas l’autorité qui vous cherchez, vous avez la possibilité d’en demander l’ajout en suivant le lien en bas de page (cf capture ci-dessus).

### Troisième étape : Formuler sa demande

La plateforme vous propose un modèle de demande, dans lequel vous n’avez qu’à indiquer le ou les document(s) que vous sollicitez.

<figure markdown="span">
![Capture d'écran: formulaire de rédaction d'une demande](media/madada-screenshot-formulaire-demande.png)
</figure>

Indiquez ici quels documents vous souhaitez obtenir, en veillant à être le plus précis possible quant à :

1. La typologie de document que vous sollicitez (un rapport, des statistiques, une délibération, un
   code source, etc.)

2. La période concernée, le cas échéant (les données de l’année 2020, la délibération du 12 juillet
   1998, etc.).

Vous pouvez bien entendu modifier ce texte à votre guise, par exemple si vous souhaitez contextualiser votre demande, ou donner des exemples de jurisprudence (ou de doctrine de la CADA) confirmant la communicabilité des documents que vous sollicitez.

### Quatrième étape : Enfin, vous pouvez prévisualiser votre demande. Vous cliquez sur valider, et c’est parti !

Votre demande est publique sur Ma Dada. Vous recevrez automatiquement
une notification, par mail, si une réponse y est apportée.

## Et après ?

### Renseignez le statut de votre demande

Au fil des événements impactant votre demande (si vous avez reçu une réponse notamment), la plateforme va vous inviter à actualiser le statut de votre
demande.

Indiquez ainsi si vous avez simplement eu un accusé de réception, si l’administration vous a communiqué le document, etc.

Cette actualisation vous permettra de mieux vous y retrouver dans vos demandes ! Cela nous aidera également à mieux vous guider dans la suite de vos démarches.

<figure markdown="span">
![Capture d'écran: formulaire de mise à jour du statut d'une demande](media/madada-screenshot-formulaire-mise-a-jour-statut.png)
</figure>

### Un suivi des demandes facilité

Afin de faciliter le suivi de vos demandes, Ma Dada vous propose un tableau de bord particulièrement pratique. Vous pouvez savoir en un coup d’œil où en sont vos demandes, et si une action de votre part est nécessaire !

<figure markdown="span">
![Capture d'écran: Écran de suivi des demandes en cours](media/madada-screenshot-suivi-demandes.png){ width="800"}
</figure>

## Ma Dada, c’est aussi...

Vous souhaitez utiliser Ma Dada à des fins de recherche, d’investigations plus poussées, ou dans un cadre militant (association, syndicat...) ? La plateforme propose des fonctionnalités spécifiques, [accessibles gratuitement sur demande](https://madada.fr/pro).

### Demandes groupées

Vous avez besoin d’envoyer la même demande à plusieurs administrations (par exemple à tous les départements de France) ?

Ma Dada vous permet d’en sélectionner autant que vous le souhaitez, parmi sa base de plus de 50 000 autorités publiques.

<figure markdown="span">
  ![Capture d'écran: Animation montrant l'ajout d'autorités à une demande groupée](media/madada-gif-demande-groupee.gif){ width="800"}
</figure>

### Demandes sous embargo

<figure markdown="span">
  ![Capture d'écran: Animation montrant la mise en place d'un embargo sur une demande](media/madada-gif-embargo.gif){ width="800"}
</figure>

Vous souhaitez que votre demande ne soit pas immédiatement rendue publique (par exemple le temps d’en analyser le contenu) ? Ma Dada vous permet de choisir une période dite d’embargo, modifiable à votre guise.

## Foire aux questions

### Quelles sont les exceptions au droit d’accès ?

Rien n’interdit de demander... Cependant, l’administration peut refuser de vous communiquer des documents, notamment si leur consultation risque de porter atteinte :

- Au secret de la défense nationale
- A la recherche et à la prévention, par les services compétents, d'infractions de toute nature
- Au secret des délibérations du Gouvernement
- A la sécurité publique ou à la sécurité des personnes
- A la monnaie et au crédit public
- A la sécurité des systèmes d'information des administrations

Certains documents ne peuvent être communiqués qu’à certaines personnes, par exemple dans le cas d’un dossier médical.
Enfin, il faut garder en tête que le droit d’accès ne porte que sur les documents dits définitifs, ce qui exclut les brouillons, mais aussi tous les documents préparatoires à une décision qui ne serait pas intervenue.

### À qui puis-je adresser ma demande ?

Vous pouvez adresser votre demande à la personne qui a produit le document et/ou à une personne qui l’a reçu. Pour rappel, le droit d’accès s’applique à tous les organismes - et même les entreprises - dès lors qu’ils sont chargés d’une mission de service public.

Mairies, ministères, services déconcentrés, départements, autorités administratives indépendantes (CNIL, AMF...), communautés d’agglomérations, CAF... sont par exemple concernés.

Vous pouvez également faire valoir ce droit auprès de certaines entreprises, à l’image de la SNCF ou d’Enedis par exemple, dès lors que les documents sollicités ont été produits ou reçus dans le cadre d’une mission de service public.

Attention cependant : l’Assemblée nationale et le Sénat ne sont pas concernés par le droit d’accès aux documents administratifs (au nom de la séparation des pouvoirs).

### Dois-je expliquer pourquoi je souhaite obtenir un document ?

Non, ce n’est pas obligatoire. Dans certains cas, cela peut cependant contribuer à un meilleur traitement de votre demande.

### Est-ce que cela va me coûter quelque chose ?

Non, cela est totalement gratuit. Seule exception : si vous sollicitez des copies occasionnant des frais de reproduction pour l’administration (s’il faut faire des photocopies par exemple). Voilà pourquoi il est généralement plus judicieux de demander des documents au format électronique.

### Comment augmenter ses chances de réussite ?

C’est surtout en facilitant le travail de l’administration que l’on peut augmenter ses chances de réussite. Dès lors, quatre règles d’or :

1.  Envoyer la demande à l’administration qui a produit le document,

2.  Identifier le plus précisément possible le ou les documents sollicités (un rapport, des statistiques, une délibération, un code source, etc.),

3.  Préciser la période concernée, le cas échéant (les données de l’année 2020, la délibération du 12 juillet 1998, etc.),

4.  S’assurer que le ou les documents demandés soient “définitifs”, c’est-à-dire qu’il ne s’agisse pas de brouillons ou que la décision qu’ils sont censés influencer ait été prise (marché public conclu, décret paru au Journal officiel, etc.).

### Que se passe-t-il une fois que ma demande a été envoyée ?

L’administration dispose d’un délai d’un mois pour vous répondre. Si vous avez obtenu le document sollicité, bravo, l’aventure s’arrête là !

Si l’administration vous a refusé l’accès au document, ou n’a pas répondu durant le délai d’un mois, vous avez la possibilité de saisir la Commission d’accès aux documents administratifs (CADA). Cette autorité indépendante fait alors office de médiateur entre l’administration et le demandeur.

Bon à savoir : ce recours est gratuit, et peut désormais s’effectuer [depuis Ma Dada](usagers/saisir-la-cada.md).

Si vous avez encore besoin d’aide et/ou de conseils, vous pouvez venir en discuter sur le forum de Ma Dada, ou nous écrire à [contact@madada.fr](mailto:contact@madada.fr) !
