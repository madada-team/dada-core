# Qui sommes-nous ?

Ma Dada est une initiative portée depuis 2019 par l’association Open Knowledge France (RNA n°W751219500).

Fondée en 2013, Open Knowledge France est une association à but non lucratif engagée dans la promotion du savoir et l’ouverture des données. Au travers du projet Ma Dada, nous croyons que l’accès aux informations et données publiques favorise la confiance des citoyens envers les élus et institutions, renforce la participation citoyenne, stimule l’innovation et améliore plus globalement la société.

# Gouvernance

L’association Open Knowledge France se structure autour d’une communauté de membres actifs et d’un conseil d’administration collégial, actuellement composé de 14 membres :

- Mathieu Caps
- Marie Castagné
- Pierre Chrzanowski (co-trésorier)
- Julia Dumont
- Elsa Foucraut
- Kevin Gernier
- Samuel Goëta (co-trésorier)
- Alexandre Léchenet
- Samuel Le Goff
- David Libeau
- Laure Lucchesi
- Noël Lucia
- Soizic Pénicaud
- Laurent Savaete

Deux personnes composent actuellement l'équipe de Ma Dada : Xavier Berne (délégué général) et Laurent Savaëte (pour la technique, notamment). La plateforme est maintenue quotidiennement, parfois avec l’aide de membres de l’association, voire d’utilisateurs de Ma Dada.

# Documents officiels

Vous pouvez retrouver en suivant les liens ci-dessous :

- [Les statuts de l’association OKF](https://fr.okfn.org/notre-groupe-local/statuts/)
- [Les rapports d’activité de l’association](https://fr.okfn.org/bilan-dactivites/)
- [Le bilan comptable 2023](https://madada.frama.space/s/jf9xWcKL3mR2HJp)
- [La fiche d’Open Knowledge France sur le registre de représentants d’intérêts de la HATVP](https://www.hatvp.fr/fiche-organisation/?organisation=798488102)

# Nos financements

Open Knowledge France a reçu les soutiens financiers suivants au titre de Ma Dada:

## My Society: 2020-2021

![mySociety](images/logo-mysociety.svg){ align=right }

En plus d'avoir développé et mis à disposition le logiciel alaveteli sur lequel Ma Dada est basé,
[mysociety](https://www.mysociety.org) a financé la mise en place des fonctions [Ma Dada++](https://madada.fr/pro) en 2020 à hauteur de 5 000 €. Ce projet était soutenu par la [fondation Adessium](https://adessium.org/).

## NLNet: 2021-2022

![logo NLnet](images/NGI0Discovery_tag.svg){ align=left }

En 2021, Ma Dada a reçu un financement de 31 600 € de NLnet dans le cadre du fonds NGI Zero Discovery. Celui-ci a reçu le support financier du programme Next Generation Internet (NGI) de la Commission Européenne, sous l'égide de la Direction Générale Réseaux de communication, contenu et technologie.

Ce financement a permis diverses améliorations du site, de la documentation et la mise en place d'un lien avec WikiData
pour enrichir les données de notre site.

## Accélérateur d’initiatives citoyennes (DINUM): 2022

![logo AIC](images/Logo_Gouvernement-DINUM_RVB.png){ align=right }

Ma Dada a fait partie de la promotion inaugurale de l'Accélérateur d'initiatives citoyenne (AIC). Celui-ci nous a
accompagnés pendant six mois et soutenu le développement de notre équipe via une subvention de 80 000 €.

## NGI Search - OC1: 2023-2024

![NGI Search](images/NGISearch_logo_tag_icon.svg){ align=left }

Sur 2023-2024, Ma Dada a reçu un financement de l'Union européenne de 119 500 €, lequel a permis d'intégrer la saisine de la CADA dans Ma Dada, d'améliorer la recherche ainsi que diverses autres fonctionnalités du site.

![Financé par l'UE](images/funded_by_eu_horizontal.png){ align=left }

Les vues et opinions exprimées sur nos sites sont celles de leurs
auteurs.trices et ne reflètent pas nécessairement celles de l'Union Européenne ou de la Commission Européenne. Ni
l'Union Européenne, ni NGI Search ne peuvent en être tenus responsables. Projet financé dans le cadre du projet NGI Search sous le numéro 101069364.

## My Society (Isocrates foundation): 2024

![logo AccessInfo](images/access-info_logo.png){ align=right }

mySociety a financé en 2024 une analyse du contexte légal français en partenariat avec [AccessInfo
Europe](https://www.access-info.org/) à hauteur de 10 000 €. L'objectif est de promouvoir le droit d'accès et la plateforme auprès des
journalistes, et de formuler des recommandations réglementaires.

## NGI Search - OC5: 2024-2025

![NGI Search](images/NGISearch_logo_tag_icon.svg){ align=left }

D’ici mars 2025, Ma Dada devrait recevoir un financement de l'Union européenne de 29 100 €. Cette aide est destinée à l’amélioration de la plateforme (mise en avant des documents obtenus, demandes “clés en main”, etc.) mais aussi à l’accompagnement des utilisateurs, notamment associatifs.

## Dons individuels

Ma Dada a aussi reçu des dons individuels à hauteur de 3 800 € entre 2022 et 2023.

Ces dons proviennent de plus de 30 personnes physiques ainsi que de La Quadrature du Net.

# Nos soutiens

L'association reçoit par ailleurs le soutien juridique de [Maître Alexis Fitzjean Ó Cobhthaigh](https://afocavocat.eu/), avocat au Barreau de
Paris.

[Gandi](https://gandi.net) héberge gracieusement le site depuis sa mise en route en 2019.

## Rejoignez-nous

En tant qu’association, nous fonctionnons principalement grâce à l’engagement de nos bénévoles, partenaires et contributeurs.

Si vous appréciez notre travail, vous pouvez contribuer en faisant un don via [Liberapay](https://liberapay.com/madada) ou [HelloAsso](https://www.helloasso.com/associations/open-knowledge-france).

Vous pouvez également donner de votre temps en contribuant autrement : support, communication, traduction...
Toute aide est la bienvenue ! N'hésitez pas à nous écrire à [contact@madada.fr](mailto:contact@madada.fr) où à rejoindre notre communauté sur notre [forum](https://forum.madada.fr).
