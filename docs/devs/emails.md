# Email setup

Email plays a central role in Ma Dada (and any alaveteli site), so we have spent a lot of time polishing it to ensure as
many emails as possible reach the people they're supposed to reach.

## Overview

Emails are sent and received with [postfix](https://www.postfix.org/) as our SMTP server. We use [dovecot](https://doc.dovecot.org/2.3/) for POP3 and IMAP.
Wherever possible, we enforce or push towards encryption with TLS, keeping in mind that some of our email senders or
recipients might be using older setups, so we allow suboptimal encryptions as well.

We essentially have 2 types of emails:

- request emails, in and out, with one "magic" address for each request on the site. These are handled by
  [Alaveteli](https://alaveteli.org) via POP3.
- our contact/support email address which functions separately, and is typically interfaced with our
  [zammad](https://zammad.org/) instance.

## Looking legit

One of the main parts of setup is being compliant with a variety of email authentication systems that help fight spam,
specifically by showing that we are not spammers when we send emails.

Most of the setup below is implemented between our server config and DNS.

### SPF

SPF allows listing which servers are permitted to send email from the `@madada.fr` domain.

More about `SPF` [here](https://github.com/internetstandards/toolbox-wiki/blob/main/SPF-how-to.md).

### DKIM

DKIM is used to cryptographically sign all outgoing emails, so that recipients can confirm the content they received is
indeed from us (we sign with a private key, they can verify with a public key posted in our DNS records).

More details [here](https://github.com/internetstandards/toolbox-wiki/blob/main/DKIM-how-to.md)

### DMARC

DMARC is used to tell receiving servers what to do with emails that fail either SPF or DKIM checks. In our case, we use
a `quarantine` policy, which tells them to move the email to the "spam" folder of recipients. This in turn increases
trust in our legitimate emails.

More [here](https://github.com/internetstandards/toolbox-wiki/blob/main/DMARC-how-to.md).

### DANE

## Validating the setup

We use various email testing services to verify that we are clean:

- https://internet.nl/mail/madada.fr/1394993/#

## Anti-spam setup

As we started receiving lost more spam once the contact alias became a standalone mailbox, we setup [rspamd](https://www.rspamd.com) as a spam filter.

### Rspamd configuration

The entry point is the
[rspamd.yml](https://gitlab.com/madada-team/dada-core/-/blob/master/ansible/roles/alaveteli/tasks/rspamd.yml?ref_type=heads)
ansible file, which links to all components of the config.

Some customisations we have done:

- add points to emails from a few countries that almost exclusively send spam to us

### webUI to help understand what rspamd does

To access the rspamd web UI, setup an SSH tunnel to the server with `ssh -L 11334:localhost:11334 -N -f
username@servername` and then open `http://localhost:11334` on your local browser. This allows tuning a variety of
settings dynamically. It is also possible to manually check emails to understand how they are assessed by rspamd.
