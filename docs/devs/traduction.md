# Traduction de Ma Dada

La version "principale" d'Alaveteli est développée en Anglais, ainsi que tous les textes du site.

Notre processus de déploiment utilise plusieurs systèmes pour traduire le logiciel en Français:

- [transifex](https://transifex.com): l'outil principal de traduction (Note: depuis Janvier 2025, nous n'utilisons plus
  transifex, nos traductions sont faites directement dans les fichiers `ansible/roles/alaveteli/files/app*.po`).
- Certaines pages (majoritairement statiques) sont traduites dans le thème alaveteli.
- certaines chaines de caractères sont traduites à la main dans notre playbook ansible lorsque le code source parent ne
  supporte pas la localisation pour un texte en particulier.

## Trouver où traduire

La logique pour trouver où traduire un bout de texte en particulier est à peu près la suivante:

- Vérifier si la chaine figure dans `ansible/roles/alaveteli/files/app*.po`. Si c'est le cas, ajuster la tranduction
  dans ce fichier.
- Dans certains cas, le texte en question n'est tout simplement pas traduisible sans modifier le code source
  d'alaveteli. Chercher "Add pagination translation" dans ce dépôt pour un exemple.

## Modifier une traduction dans les fichiers app.po

Ma Dada n'utilise plus [transifex](https://transifex.com) parce que nos traductions étaient perdues lors de mises à jour
majeures d'alaveteli.

Les traductions sont séparées en deux fichiers dans le dossier `ansible/roles/alaveteli/files/`.

- `app_pro.po` qui inclut tout ce qui touche aux fonctions "Pro" (Ma Dada++),
- `app.po` qui contient tout le reste.

Il est parfois un peu difficile de trouver où se cache un morceau de texte en particulier. Chercher dans les deux
fichiers est souvent une bonne idée, même s'il semble "évident" où le texte devrait se trouver.

!!! tip "Attention aux traductions"

    Attention en particulier à ne pas traduire mot à mot: les réglementations brittanique (pour lesquelles le texte
    original est écrit) et française sont assez différentes, et il faut parfois adapter le texte au contexte de la page
    sur laquelle il apparaît.

    Nous avons créé une [page de terminologie](/devs/terminologie) pour Ma Dada qui recense les expressions les plus couramment utilisées avec
    leurs équivalents originaux si nécessaire.
