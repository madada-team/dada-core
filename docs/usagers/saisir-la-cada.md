# Saisir la CADA

Vous n’avez pas obtenu le document que vous souhaitiez (ou seulement une partie des documents) ? Si l’administration vous a opposé un refus, ou n’a apporté aucune réponse à votre demande dans un délai d’un mois, vous avez la possibilité de saisir la Commission d’accès aux documents administratifs (CADA). Cette autorité indépendante, qui fait office de médiateur entre l’administration et le demandeur, peut en effet se prononcer sur les refus explicite (réponse écrite de l'administration) ou implicite (silence conservé pendant un mois).

Bon à savoir : ce recours est gratuit, et peut désormais s’effectuer depuis Ma Dada.

Voici les étapes à suivre :

1. Délai de saisine : Vous devez saisir la CADA dans un délai de deux mois suivant la notification du refus ou de l’intervention du refus implicite. Si l’administration ne vous a pas notifié les délais et voies de recours (ce qui est normalement obligatoire), ce délai peut être porté à un an.

2. Dépôt de la demande d’avis : En pratique, le recours devant la CADA se traduit par une “demande d’avis”. Le formalisme est très réduit, puisqu’il suffit de fournir ses coordonnées, d’indiquer sur quel(s) document(s) la demande portait, et de fournir un justificatif de la demande (et de l’éventuelle réponse apportée par l’administration). Rassurez-vous : Ma Dada simplifie au maximum ce processus, si bien que vous n’avez qu’à renseigner vos informations personnelles et rappeler sur quel(s) document(s) la demande portait.

## Pourquoi saisir la CADA ?

De nombreuses administrations ne répondent jamais aux demandeurs, parfois dans l’espoir que ceux-ci se découragent et abandonnent tout simplement. La CADA donne d’ailleurs souvent gain de cause aux demandeurs (59 % d’avis favorables en 2023 par exemple).

La CADA n’a pas de pouvoir contraignant : ses avis demeurent consultatifs \- ce qui signifie que l’administration n’est pas obligée de les suivre. L’autorité jouit néanmoins d’un poids moral de nature à inciter les administrations à suivre ses avis.

## Dans quels cas saisir la CADA ?

Si les cas de refus explicite (réponse écrite de l'administration) ou implicite (absence de réponse dans un délai d’un mois) sont les plus évidents, sachez qu’il est également possible de saisir la CADA lorsque vous n’obtenez que partiellement satisfaction. Typiquement, si vous n’obtenez qu’un document alors que vous en aviez demandé plusieurs.

Vous pouvez également saisir la CADA si les documents sollicités ne vous ont pas été communiqués dans le format sollicité (sachant que l’administration doit théoriquement les communiquer via Ma Dada dans “_un standard ouvert, aisément réutilisable et exploitable par un système de traitement automatisé_” \- cf article [L300-4](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033205519) du CRPA).

## Sous quel délai aurais-je une réponse ?

Bien que la CADA soit tenue de rendre ses avis dans un délai d’un mois, il est fort probable que vous ayez besoin de patienter davantage (le délai moyen était de quasiment deux mois en 2022). Sachez que si la CADA ne s’est pas prononcée dans un délai de deux mois (à compter de l’avis d’enregistrement de votre saisine), vous pouvez saisir directement le juge administratif.

## Que va faire la CADA ?

Dans une logique de contradictoire, la CADA demande à l’administration mise en cause de fournir ses observations. A l’aune de ces éléments, la CADA statue sur le bien-fondé du refus.

La CADA peut émettre un avis favorable si elle estime que le document est communicable, ou défavorable dans le cas
contraire. Si la demande portait sur plusieurs documents, l'avis peut contenir plusieurs points, pour chacun de ces
documents.

Dans certains cas, la CADA peut se déclarer incompétente, en particulier si la demande initiale ne s'appuie pas sur le
droit d'accès aux documents administratifs.

L’avis est ensuite transmis au demandeur, comme à l’administration. Cette dernière est tenue d’informer la CADA, dans un délai d’un mois, des suites qu'elle entend donner à la demande.

Les échanges entre la CADA et l’administration sont évidemment des documents administratifs, bien souvent communicables.

## Que faire si l’administration persiste dans son refus, malgré un avis favorable de la CADA ?

Vous pouvez commencer par envoyer un message (de suivi, via Ma Dada) pour inviter l’administration à vous communiquer le document sollicité, conformément à l’avis de la CADA \- en rappelant dans l’idéal le numéro et la date de l’avis CADA. L’envoi d’un message similaire en recommandé peut également aider.

Le seul véritable recours demeure néanmoins de saisir le juge administratif.

Pour plus d’informations, vous pouvez consulter le site de la CADA, et notamment [cette page](https://www.cada.fr/particulier/quand-et-comment-saisir-la-cada).
