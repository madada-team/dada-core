# Charte d'usage

Ma Dada est une plateforme opérée par l'association [Open Knowledge France](https://fr.okfn.org/). Elle vise à faciliter la demande de documents administratifs communicables à tous.

En créant un compte sur madada.fr, vous vous engagez à adhérer à la présente charte.

La violation de l'une des règles ci-dessous est susceptible d'entraîner la suspension de votre compte, ainsi que la suppression de votre demande.

Le respect de ces règles est important, notamment pour éviter l’intervention des bénévoles de l’association Open Knowledge France, et par là même la capacité de cette dernière à maintenir le service.

## Les utilisateurs

En utilisant Ma Dada, vous vous engagez à :

- Ne pas utiliser le service pour des demandes à caractère privé, visant à l'obtention de documents contenant des données privées et/ou nominatives.
- Ne pas utiliser le service pour envoyer des courriels non-désirés (spam) aux autorités publiques.
- Utiliser un ton courtois lors de vos échanges avec les autorités publiques saisies.
- Ne pas menacer, diffamer ou insulter vos interlocuteurs.

Si vous rencontrez un problème dans l'utilisation du service ou que vous avez une question, vous pouvez contacter l’équipe de Ma Dada via [ce formulaire](https://madada.fr/aide/contact).

## Les autorités publiques

Lorsque vous répondez à une demande d'accès à un document administratif, vos réponses sont publiques. Il est donc important que vous vérifiez le contenu de vos emails (signature, etc.) afin d'éviter que des données nominatives ou personnelles dont vous ne souhaiteriez pas la diffusion publique y soient mentionnées.

Lorsque vous recevez une demande d'accès à un document administratif, merci de répondre à l’adresse email fournie par notre service.

Si vous rencontrez un problème dans l'utilisation du service ou que vous avez une question, vous pouvez contacter l’équipe de Ma Dada en utilisant [ce formulaire](https://madada.fr/aide/contact).

## Les fonctionnalités Ma Dada++

Ma Dada dispose d'une version augmentée, nommée Ma Dada++.
Pour en bénéficier, il faut en faire la demande par le biais de [ce formulaire](https://madada.fr/pro) et remplir les conditions suivantes :

- Avoir créé un compte sur Ma Dada et effectué au moins une première demande
- Justifier du besoin d'utilisation de ces fonctionnalités avancées (recherches, enquête journalistique, etc.)

Une fois votre compte Ma Dada++ activé, vous pourrez envoyer la même demande à plusieurs autorités administratives simultanément. D’autre part, vous pourrez choisir de masquer la visibilité de vos demandes d'accès à des documents administratifs en plaçant celles-ci sous embargo pour la durée de votre choix (3, 6 ou 12 mois). Cette fonctionnalité est destinée notamment aux journalistes ou chercheurs, pendant la durée de leur travail d'investigation. Toutefois, à l'issue de cette période, vous vous engagez à rendre public vos demandes car le but de Ma Dada est de contribuer à l'accès ouvert aux informations publiques.
Afin de conserver la crédibilité de notre serveur mail, nous rappelons que celui-ci ne doit pas être utilisé à d’autres fins que les demandes d'accès à des documents administratifs. Aussi, le demandeur s’engage à consacrer le temps nécessaire au traitement des réponses apportées à ses demandes (lecture, actualisation des statuts…), qu’il lui conviendra d’anticiper.

## Modération

Nous adoptons une position neutre vis-à-vis des documents demandés aux autorités publiques et ne soutenons aucune cause partisane ou politique.

L'équipe d'administration de Ma Dada - composée de membres de l’association Open Knowledge France - est au service de ses utilisateurs (qu’ils soient des demandeurs ou des administrations), afin de faciliter l'exercice du droit d'accès aux informations publiques.

Cette équipe se réserve la possibilité de masquer ou de supprimer toute demande contrevenant aux règles énoncées ci-dessus (propos injurieux ou diffamants, demandes à caractère personnel, etc.). Tout contenu manifestement illicite, notamment au regard de la législation applicable aux documents administratifs, pourra également être retiré.

Si vous constatez une erreur dans l’annuaire des autorités publiques de Ma Dada ou que vous souhaitez l'ajout d'une nouvelle autorité publique, merci d'utiliser [ce formulaire](https://madada.fr/aide/contact).

Ma Dada est une plateforme associative. Bien que nous fassions notre possible pour faciliter votre droit d’accès, nous ne pouvons en aucun cas garantir le résultat de vos demandes, ni notre capacité à intervenir dans des délais précis.
