# Utilisation de Ma Dada

Cette section va vous aider à mieux comprendre l’utilité et les fonctionnalités de Ma Dada.
Il existe également trois vidéos tutoriels introductifs à Ma Dada sur notre chaîne Peertube, qui peuvent vous aider :

- Vidéo tutoriel [Qu'est-ce que le droit d'accès aux documents administratifs
  ?](https://aperi.tube/w/57JyF8gutgFJJKn7fd5Yeh)
- Vidéo tutoriel [Qu'est-ce que Ma Dada ?](https://aperi.tube/w/9Bfj6KdE52M9Cj99HYhHAk)
- Vidéo tutoriel [Comment faire une demande d'accès aux documents administratifs avec Ma Dada ?](https://aperi.tube/w/ibnzJp7pEJ113MdVCmPBpA)

## Etape 1 : Créer un compte / Se connecter

S’il n’est pas nécessaire d’être inscrit pour consulter les demandes faites par d’autres utilisateurs sur Ma Dada, il faut avoir un compte pour pouvoir envoyer sa propre demande.

Pour créer un compte utilisateur, il suffit de remplir ce [formulaire](https://madada.fr/profile/sign_in).

Attention au moment de remplir le champ “Votre nom” : ce nom apparaîtra publiquement sur Ma Dada (et peut-être dans [certains moteurs de recherches](https://madada.fr/aide/confidentialite#public_request)).

Vous recevrez ensuite un mail contenant un lien d'activation et de confirmation de la création du compte.

## Etape 2 : Chercher l’autorité à laquelle envoyer votre demande

Une fois connecté, vous devez sélectionner une autorité (mairie, ministère, etc.) à partir de la barre de recherche, ou [via cette page](https://madada.fr/selectionner_autorite). Si l'autorité administrative n'existe pas, vous devez utiliser le formulaire afin de demander son ajout sur Ma Dada ([voir la procédure](https://doc.madada.fr/usagers/ajouter-une-autorite/))

Vous pouvez également parcourir la [liste des autorités administratives](https://madada.fr/body/list/all) classées alphabétiquement et utiliser les catégories disponibles sur la gauche de la liste afin de filtrer des éléments.

Afin d'obtenir de meilleurs résultats lors de l’utilisation du moteur de recherche, vous pouvez utiliser des syntaxes particulières ([voir la liste des expressions filtrant les résultats de recherche](https://madada.fr/advancedsearch)). Parmi ces expressions, l'utilisation du mot clé `tag:` (avec les deux points) vous permet de récupérer la liste des autorités publiques appartenant à la même catégorie (par exemple : `tag:cg` permet d'obtenir la liste de l'ensemble des conseils départementaux).

En cliquant sur le nom de l'autorité administrative que vous cherchiez, vous pouvez prendre connaissance des demandes de communications qui lui ont déjà été adressées sur notre site. Cette étape est importante : peut-être qu’un autre utilisateur a déjà obtenu le document que vous recherchiez.

Vous devez enfin **cliquer sur le bouton "Faire une demande"** afin d'initialiser votre demande.

## Etape 3 : Rédiger votre demande

Votre intervention est nécessaire sur deux points :

1. Ma Dada vous demande de donner un “Résumé” de votre demande. Il s’agit de l’objet de votre demande (des notes de frais, une convention…). Ce titre doit être court tout en permettant d'identifier rapidement l'objet de votre demande. Nous vous invitons donc à être malgré tout précis, en privilégiant par exemple “Notes de frais 2020 du maire de Paris” plutôt que simplement “Notes de frais - maire”.
2. Vous devez ensuite préciser “Votre demande”. Ma Dada vous propose un formulaire pré-rempli, où vous n’avez plus qu’à préciser le ou les documents que vous souhaitez obtenir.

Ce modèle contient les principales dispositions censées contribuer au bon traitement de votre demande (rappel du cadre légal, etc.). Vous restez cependant libre de modifier ce texte, notamment si vous souhaitez ajouter du contexte.

Quelques conseils néanmoins :

- Identifier le plus précisément possible le ou les documents sollicités (un rapport, des statistiques, une délibération, un code source, etc.)
- Préciser la période concernée, le cas échéant (les données de l’année 2020, la délibération du 12 juillet 1998, etc.).
- S’assurer que le ou les documents demandés soient “définitifs”, c’est-à-dire qu’il ne s’agisse pas de brouillons ou que la décision qu’ils sont censés influencer ait été prise (marché public conclu, décret paru au Journal officiel, etc.).

Si vous souhaitez plusieurs documents sans lien direct entre eux, nous vous conseillons de faire plusieurs demandes (dans l’idéal une demande par typologie de document).

Exemple : faire une première demande pour des documents relatifs à un marché public (factures, offres de prix des candidats, méthode de notation…), et une seconde pour des dossiers de demandes de subvention reçues par la même mairie - ceci plutôt qu’une seule demande ciblant l’ensemble de ces documents.

## Etape 4 : Envoyer (puis suivre) votre demande

Une fois votre demande rédigée, vous pouvez l'adresser à l'autorité sélectionnée. Cette demande lui sera transmise par le biais d'une adresse email unique générée par la plateforme. L'autorité ne verra donc pas votre adresse email personnelle. Ce mécanisme permet d'acheminer automatiquement tous les échanges entre vous et l'autorité publique saisie sur le fil de discussion public dédié.

En complément, le système calcule les dates d'échéances réglementaires et vous informe des actions à réaliser.

En retour, il vous est simplement demandé de tenir à jour le statut de votre demande.

Lorsque votre demande est en cours :

- Je suis encore en attente d'une réponse : les autorités publiques utilisent parfois des systèmes de filtrage anti-spams et vous demandent de valider votre email en saisissant un code (“captcha”) sur une page Internet. Dans ce cas, il vous appartient d'effectuer cette tâche et de mettre à jour le statut de votre demande en indiquant que vous êtes toujours en attente d'une réponse.
- J'ai été invité à préciser ma demande : parfois, les autorités publiques vous demandent de préciser votre demande si elles estiment que celle-ci est trop imprécise. Il vous appartient de leur répondre et de mettre à jour votre demande initiale avec ce statut.
- Ils vont répondre par courrier postal : les autorités publiques choisissent parfois de répondre à une demande en ligne par un courrier postal. Dans ce cas, il n'est pas possible de continuer à suivre les échanges via notre site. Il vous suffit de mettre à jour votre demande avec ce statut.

Il existe également d'autres évènements nécessitant la mise à jour du statut de votre demande :

- J'ai reçu un message d'erreur : pour diverses raisons, le mail envoyé à l'autorité publique peut ne pas lui avoir été transmis.
- Cette demande requiert une attention des administrateurs : en utilisant ce statut, vous pouvez signaler aux administrateurs du site ce problème afin qu'ils essaient de le résoudre.
- Je voudrais retirer cette demande : vous avez soumis une demande que vous souhaitez supprimer. En utilisant ce statut, votre requête sera retirée de l'affichage public et un message sera transmis à l'autorité publique concernée.

## Etape 5 : Et après ?

En cas de réponse négative ou d’absence de réponse à l'issue du délai légal d’un mois, vous avez la possibilité de saisir la Commission d’accès aux documents administratifs (CADA).

En cas de réponse positive, bravo, l’aventure s’arrête là pour vous ! Ma Dada vous permet néanmoins d’écrire un email de remerciement à l'autorité publique saisie.

Dans un cas comme dans l’autre, nous vous invitons à mettre à jour le statut de votre demande :

- Ils ne détiennent pas l'information : parfois, l'autorité publique contactée peut vous répondre en indiquant qu'elle ne détient pas le document demandé. Si vous pensez qu’une autre administration peut avoir ce document, vous pouvez soit envoyer une nouvelle demande à cette autorité, soit rappeler à votre interlocuteur que l’article L114-2 du CRPA prévoit que "Lorsqu'une demande est adressée à une administration incompétente, cette dernière la transmet à l'administration compétente et en avise l'intéressé."
- J'ai reçu une partie des informations : parfois, vous n’obtiendrez qu'une partie des documents demandés. Afin de distinguer les demandes abouties des demandes partiellement abouties, il est utile de mettre à jour votre demande avec ce statut le cas échéant.
- J'ai reçu toutes les informations : Bravo ! Merci de nous le faire savoir : en mettant à jour le statut de votre demande, cela encouragera d'autres utilisateurs à utiliser ce service en s'inspirant éventuellement de votre succès. Cela nous est également utile pour notre suivi interne.
- Ma demande a été refusée : même s’il s’agit d’une mauvaise nouvelle, merci d’actualiser votre statut, ne serait-ce que pour les statistiques du site.

## Comment demander l'ajout d'une autorité administrative ?

Lorsque l'autorité administrative à laquelle vous souhaitez vous adresser n'existe pas sur Ma Dada, vous pouvez utiliser notre [formulaire de demande de création](https://madada.fr/change_request/new) afin que nous l'ajoutions.

Vous devez indiquer son nom, une adresse email de contact, l'adresse Internet au sein de laquelle on peut trouver des informations la concernant et éventuellement d'autres coordonnées de contact (adresse postale, numéro de téléphone).

Il n’est pas toujours simple de trouver l’adresse mail d’une administration. Cette information figure parfois dans la page "Mentions légales".

Votre formulaire est transmis à l’équipe de Ma Dada, qui va valider votre demande et ajouter l'autorité publique suggérée. En cas d'absence d'email de contact, nous allons essayer de trouver cette information pour ajouter l'autorité publique suggérée.

## Créer un compte Ma Dada++

Les comptes ++ disposent principalement de deux fonctionnalités supplémentaires à celles des comptes classiques :

- La possibilité de prévoir une période d’embargo, durant laquelle la demande n’est pas rendue publique
- La possibilité d’envoyer la même demande à un groupe d’autorités (éventuellement en sélectionnant selon des catégories : ministères, mairies, etc.)

L’octroi de ces fonctionnalités est soumis à deux conditions :

1. L’utilisateur doit déjà avoir un compte, et effectué au moins une demande via Ma Dada
2. L’utilisateur doit justifier de son besoin de profiter de telles fonctionnalités (une enquête journalistique, un travail de recherche, etc.)

L'accès aux comptes Ma Dada ++ est gratuit, mais nous le réservons pour l’instant (pour des raisons de sécurité et de ressources) à des personnes en ayant un besoin avéré.

Vous pouvez nous en faire la demande à [contact@madada.fr](mailto:contact@madada.fr),ou via ce [formulaire de
demande](https://madada.fr/pro#input-account-request).
