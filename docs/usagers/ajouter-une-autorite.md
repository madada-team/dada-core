# Comment demander l'ajout d'une autorité administrative ?

En tant qu'utilisatrice ou utilisateur, je souhaite demander l'ajout d'une nouvelle autorité publique afin de lui demander la communication d'un document administratif.

Lorsque l'autorité administrative à laquelle vous souhaitez vous adresser n'existe pas sur Ma Dada, vous pouvez utiliser notre [formulaire de demande de création](https://madada.fr/change_request/new) afin que nous l'ajoutions.

Vous devez indiquer son nom, une adresse email de contact, l'adresse Internet au sein de laquelle on peut trouver des informations la concernant et éventuellement d'autres coordonnées de contact (adresse postale, numéro de téléphone).

![illustration du formulaire de demande de création d'autorité publique](../media/form-create-autority.png)

**Les autorités publiques sont dans l'obligation de publier sur leur site Internet les coordonnées de contact de la personne responsable de l'accès aux documents administratifs au sein de leur organisation** suivant les dispositions du Code des relations entre le public et l'administration (CRPA article [330-3](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031370451/2016-01-01)).

Cette information figure parfois dans la page "Mentions légales". En l'absence de coordonnées précises ou dans le cas où l'autorité publique suggère l'utilisation d'un formulaire pour les prises de contact, vous pouvez soumettre une adresse email trouvée sur le site (celle du délégué à la protection des données personnelles par exemple) ou soumettre le formulaire sans mention d'adresse email.

Votre formulaire est transmis aux animatrices et animateurs de Ma Dada qui vont valider votre demande et ajouter l'autorité publique suggérée. En cas d'absence d'email de contact nous allons essayer de trouver cette information avant d'ajouter l'autorité publique suggérée.
