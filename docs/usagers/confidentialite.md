# Mentions légales

Hébergement et responsabilité éditoriale #

- Hébergeur: [Gandi SAS](https://www.gandi.net/), 63-65 boulevard Masséna, 75013 Paris
- Editeur: [OpenKnowledge France](https://fr.okfn.org/)

# Logo et images

Logo : déclinaison de [Horse](https://thenounproject.com/search/?q=horse&i=853134) réalisé par Nook Fulloption pour le Noun Project et disponible sous la licence CC-BY.

Page d'accueil : [Louvre Palace](https://commons.wikimedia.org/wiki/File:Louvre_Palace.jpg) disponible sur Wikimedia Commons sous la licence CC-BY-SA

# Politique de confidentialité et de sécurité - données personnelles et cookies

Ma Dada est gérée par OpenKnowledge France, une association loi 1901. Pour obtenir plus d'informations sur l'association, visitez [notre site web](https://fr.okfn.org/).

Nous attachons la plus grande importance à la transparence publique, sans oublier ni sacrifier la protection de la vie privée, et la sécurité de notre plateforme. C'est pourquoi nous avons longuement réfléchi pour élaborer et notre politique de confidentialité et notre conformité au Réglement Général sur la Protection des Données (RGPD), que ce soit vis à vis des utilisateurs et utilisatrices de Ma Dada, ou vis à vis des personnes travaillant pour les administrations publiques qui intéragissent sur notre plateforme.
Au dela des réglementations, nous avons développé et déployé sur Ma Dada des mécanismes pour protéger la vie privée et les informations personnelles ou sensibles qui pourraient y apparaître, ainsi que des mécanismes pour que les utilisateurs et utilisatrices puissent nous signaler ces contenus. Des mécanismes pour assurer la sécurité de la plateforme sont constamment développés, revus et corrigés : ainsi le dernier audit de sécurité poussé sur l'ensemble des outils et infrastructures de la plateforme Ma Dada à été réalisé au printemps 2022 par la plateforme indépendante spécialisée en sécurité [Radically Open Security](https://www.radicallyopensecurity.com/). L'ensemble de notre politique de sécurité et de confidentialité a été conçu par l'équipe de Ma Dada, avec l'aide de nos utilisateurs et utilisatrices, de spécialistes et juristes de ces questions, ainsi qu'avec l'aide et la consultation de la [CADA (Commission d'Accès aux Documents Administratifs)](https://cada.fr) et de la [CNIL (Commission Nationale Informatique et Libertés)](https://cnil.fr). Cette politique de confidentialité et de sécurité est régulièrement revue, corrigée et améliorée.

Nous espérons que cette section couvre l'ensemble des questions et problématiques concernant notre politique de confidentialité et de sécurité. Si ce n'est pas le cas, n'hésitez pas à nous contacter pour nous signaler tout manquement éventuel, ou pour nous faire des suggestions que nous ne manquerons pas de considérer. De même, n'hésitez pas à nous proposer votre aide, nous sommes un association, notre équipe et essentiellement bénévole, et toute aide ou envie d'implication est la bienvenue.

## Quelles informations obligatoires dois-je fournir pour la création de compte sur Ma Dada ?

En règle générale, selon le principe de minimisation des données, la plateforme Ma Dada ne vous demandera de fournir que le strict minimum des informations nécéssaires à son utilisation. Pour la création d'un compte Ma Dada il ne sera nécéssaire de fournir qu'une adresse email de connexion au site ou de contact pour le suivi automatique de vos demandes, et un nom d'utilisateur (que vous êtes libres de choisir, pseudonyme ou autre).

## Qui peut voir mon adresse email ?

Nous ne divulguerons pas votre adresse email publiquement (sauf si nous y sommes obligés légalement). Cela inclut l'autorité publique à laquelle vous envoyez une demande qui ne verra qu’un email “@madada.fr” qui aura été créé spécifiquement pour votre demande. Chaque demande d'accès aux documents administratifs est ainsi envoyée avec une nouvelle et unique adresse email de la forme ci-dessus.

Si vous envoyez un message privé via le site à un autre utilisateur, ce dernier verra votre adresse email. Un avertissement vous le rappellera au moment de l'envoi.

## Allez-vous envoyer du spam sur mon adresse email ?

Non. Une fois que vous vous inscrivez sur Ma Dada, nous ne vous enverrons que des emails qui concernent une demande que vous avez faite, une alerte email à laquelle vous vous êtes inscrit, l’activité de la plateforme ou à d'autres occasions pour lesquelles vous avez donné votre autorisation. Nous ne donnerons ou ne vendrons jamais votre adresse email à des tiers, à moins que nous soyons obligés légalement de le faire.

## Pourquoi mon nom et ma demande apparaissent publiquement sur le site ?

Nous publions votre demande sur Internet, afin que tout le monde puisse la lire et puisse utiliser les informations que vous avez reçues des autorités publiques. Normalement, nous ne supprimons pas les demandes (plus de détails voir (Pouvez-vous supprimer mes informations personnelles ?).

Votre nom est lié à votre demande, c'est pourquoi il est publié. L'utilisation de votre vrai nom aide également les personnes à entrer en contact avec vous afin de vous aider dans votre recherche. Légalement vous devez utiliser votre nom réel pour que la demande soit valide. Lisez la prochaine question pour avoir des solutions si vous ne voulez pas publier votre nom complet.

<a name="pseudonyme"></a>

## Puis-je faire une demande d'accès aux documents administratifs sous un pseudonyme ?

Vous devez utiliser votre vrai nom pour que votre demande soit valide. Cependant, les administrations devraient accepter de répondre à une demande faite sous un pseudonyme. Attention cependant, même si l'autorité suit cette bonne pratique, l'usage d'un pseudonyme vous empêchera de saisir la Commission d’Accès aux Documents Administratif (CADA) en cas de refus de l’autorité.

Il y a plusieurs alternatives à l'utilisation d'un pseudonyme:

- Nous demander via [contact@madada.fr](mailto:contact@madada.fr) de censurer sur votre compte, sur toute vos demandes ou sur une demande en particulier les informations personnelles que vous souhaiter y voir apparaitre : nom prénom, adresse, téléphone, etc. Nous indiquer la forme exacte d'apparition de ces informations personnelles sur votre compte ou demandes. L'équipe de Ma Dada procédera alors à la création d'une règle automatique de censure qui pourra censurer automatiquement toute occurrence de ces informations personnelles sur, selon la portée que vous nous indiquerez, tout votre compte Ma Dada donc la totalité de vos demandes et réponses à celles-ci, ou uniquement sur une demande en particulier (nous préciser la portée de la censure souhaitée dans ce mail).
- Utiliser une autre forme de votre nom. «M. Arthur Thomas Roberts" peut s'appeler "Arthur Roberts", "A. T. Roberts", ou "M. Roberts", mais pas "Arthur" ou "A.T.R.".
- Utiliser le nom de jeune fille.
- Utiliser, avec leur accord, le nom d'une organisation, le nom d'une société, le nom commercial d'une entreprise...
- Demander à quelqu'un de faire la demande à votre place.

Vous pouvez, si vous êtes vraiment bloqués, nous demander de faire la demande à votre place. Dans ce cas veuillez nous contacter en nous expliquant les raisons pour lesquelles vous ne pouvez pas faire la demande vous-même. Nous ne disposons pas des ressources nécessaires pour le faire pour tout le monde, donc nous essayerons de faire de notre mieux. Vous pourrez également demander à notre communauté d'utilisatrices et utilisateurs sur le [forum de Ma Dada](https://forum.madada.fr/) de faire cette demande à votre place Dans tous les cas ne vous faites jamais passer pour quelqu'un d'autre !

## L’autorité publique demande mon adresse postale

Le code des relations entre le public et l'administration (CRPA) exige que les demandes soient introduites par écrit. La jurisprudence reconnaît l’email comme un écrit valide pour demander l’accès aux informations détenues par les autorités publiques. Insistez donc pour recevoir une réponse par email.

## L’autorité a besoin d'une adresse postale pour m'envoyer une réponse par courrier postal !

Si une autorité ne dispose que d'une copie papier de l'information que vous voulez, ils peuvent vous demander une adresse postale. Pour commencer, essayez de les convaincre de numériser les documents pour vous. Si cela ne fonctionne pas, et que vous voulez fournir votre adresse postale en privé afin de recevoir les documents, marquez votre demande en utilisant l’option "Ils vont répondre par courrier postal". Ma Dada va ensuite vous fournir une adresse électronique pour les contacter en aparté et leur fournir cette adresse postale.

## Pouvez-vous supprimer mes demandes, ou modifier mon nom ?

Ma Dada est une archive publique permanente des demandes d'accès à l'information. Même si vous trouvez qu'une réponse à une demande n'est plus utile, elle peut être intéressante pour d'autres personnes. Pour cette raison, nous ne prenons pas à la légère les demandes de suppression de demandes.

Dans des circonstances exceptionnelles, nous pouvons supprimer ou modifier votre nom sur le site, (voir la question suivante). De même, nous pouvons également supprimer d'autres informations personnelles. Si vous êtes inquiet à ce sujet, consultez la section sur les pseudonymes avant de faire votre demande.

## Pouvez-vous supprimer mes informations personnelles ?

Si vous voyez des informations personnelles vous concernant sur le site et que vous souhaitez que nous les supprimions ou les cachions (censurions), veuillez nous en informer. Indiquez exactement quelles informations vous posent problème, et à quel endroit elles apparaîssent sur le site. Dites-nous également si vous souhaitez que nous les supprimions définitivement, où si vous souhaitez que nous appliquions dessus une régle de censure qui va les cacher publiquement sur la plateforme, mais qui laissera la demande intacte pour l'administration qui la recevra. Nos règles de censure s'appliquent automatiquement au choix sur tout votre compte (l'ensemble de vos demandes et les réponses obtenues) ou sur une demande spécifique (et les réponses à cette demande). Veuillez nous indiquer la portée désirée de la censure. Attention : les régles de censure automatique sur Ma Dada ne se déploient pas bien sur les documents pdf ou autres formats reçus en pièce jointe avec les réponses. Soyez attentifs et signalez-nous ces cas là pour que nous puissions trouver une solution ensemble.

Si ce sont des informations personnelles vous concernant qui ont été accidentellement postées, nous les supprimerons ou censurerons publiquement. Pour des informations personnelles concernant d'autres personnes, nous serions reconnaissant à quiconque de nous les faire remarquer. Notre équipe de modérateurs va alors se charger de les traiter dans les meilleurs délais possibles.

Pour en savoir plus, consultez la FAQ pour les administrations

## J'ai demandé à censurer mon nom, mais je le vois toujours !

Le contenu des pages du site varie selon votre statut de connexion (vous voyez plus de détails sur les
demandes que vous avez rédigées). Il est recommandé de vérifier ce que le public (ou un moteur de recherche) peut voir en accédant à une page donnée via un navigateur en mode
privé/incognito.

Concernant les moteurs de recherche, ceux-ci mettent à jour leur index à intervalle régulier ( sur lequel nous n'avons aucun contrôle). Lorsque vous demandez à cacher votre nom, il peut s'écouler quelques jours avant que les moteurs de recherches le prennent en compte.

Si malgré tout vous voyez encore votre nom sur le site, il peut s'agir d'un bug. Contactez-nous en mentionnant l'adresse de la
page en question afin que nous corrigions le problème.

# Cookies et services externes

## Cookies

Afin de rendre notre service plus facile d'utilisation et plus utile, nous utilisons de petits fichiers enregistrés sur votre navigateur web (sur votre ordinateur ou smartphone), appelés "cookies". C'est une pratique très courante, et la plupart des sites web utilisent ce système.

Ces cookies nous permettent par exemple de savoir que vous êtes connecté.e, et ainsi de ne pas vous redemander un mot de passe à chaque page. La liste ci-dessous décrit les cookies que nous utilisons:

| Nom                        | Contenu typique                                                                                      | Expiration                                                                                                                     |
| -------------------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| `_wdtk_cookie_session`     | Un identifiant unique aléatoire                                                                      | Lorsque le navigateur est fermé, ou après un mois, si vous avez coché "Souvenez-vous de moi" lorsque vous vous êtes connecté.e |
| `seen_foi2`                | Le chiffre 1 si vous avez vu une notification                                                        | 7 jours                                                                                                                        |
| `has_seen_country_message` | Le chiffre 1 si vous avez vu le message concernant les services FOI dans d'autres pays               | 1 an                                                                                                                           |
| `last_request_id`          | Un numéro identifiant la dernière demande que vous avez vue sur notre site                           | A la fermeture du navigateur                                                                                                   |
| `last_body_id`             | Un numéro identifiant la dernière administratio que vous avez vue sur notre site                     | A la fermeture du navigateur                                                                                                   |
| `widget_vote`              | Un identifiant aléatoire relatif à un vote "Je veux aussi savoir" sur une demande qui vous intéresse | A la fermeture du navigateur                                                                                                   |

## Statistiques d'utilisation

Nous utilisons le logiciel libre [Matomo Analytics](https://matomo.org/) pour collecter des statistiques anonymes sur l'utilisation du site, dans le but d'améliorer le service. Nous avons désactivé l'usage des cookies sur Matomo.
Contrairement à d'autres logiciels similaires très répandus, Matomo fait partie des [logiciels conseillés](https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/cookies-solutions-pour-les-outils-de-mesure-daudience) par la CNIL. Matomo sur Ma Dada suit les règles de configuration recommandées par la CNIL [ici](https://www.cnil.fr/sites/default/files/atoms/files/matomo_analytics_-_exemption_-_guide_de_configuration.pdf), en vue de minimiser les données receuillies et d'assurer la conformité à la legislation et au RGPD.

Les statistiques anonymes que nous conservons sont :

- L'adresse IP, tronquée à 24 bits (anonyme, sans lien avec l'identité unique d'un utilisateur ou utilisatrice)
- Les pages visitées
- L'origine de la visite
- Des informations sur votre navigateur web (logiciel, version, etc)
- Ces données ne sont ni partagées, ni agrégées avec celles d'autres sites. L'historique des visites est détruit tous les 6 mois.

Vous pouvez consulter la politique de confidentialité de Matomo sur [leur site (en anglais)](https://matomo.org/privacy-policy/).
