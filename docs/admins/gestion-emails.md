Les emails étant une partie centrale du système de Ma Dada, les problèmes sont relativement fréquents. Cette section
essaie de recenser les plus communs, et de propose une solution. Gardez à l&apos;esprit que la source du problème n'est
pas forcément chez Ma Dada, et qu'une bonne partie des erreurs ne peut être résolue que par les gestionnaires d'autres
serveurs.

!!!abstract "Petit point de vocabulaire"

    **mail sortant** ("outgoing message" en anglais): mail envoyé par le serveur de Ma Dada vers un tiers, souvent l'administration.

    **mail entrant** ("incoming message"): mail reçu par notre serveur, en général de l'administration. C'est en général une réponse à un mail sortant.

### Déterminer l'état de livraison d'un mail

#### Etat de livraison

Il est accessible depuis la page publique d'une demande, en haut à droite de chaque message envoyé.

Ces états sont automatiquement déterminés par alaveteli en lisant les logs de notre serveur mail. Ils ne sont pas
toujours corrects!

Quatre états sont possibles:

`Réceptionné`

: le serveur mail du destinataire confirme avoir reçu le message. Cela ne signifie pas que le
destinataire l'a reçu "dans sa boite mail". Le message peut-être parqué dans une zone d'attente. Mais à partir du
moment où le message a ce statut, les problèmes ne sont plus de notre ressort (sauf exceptions listées plus bas,
anti-spam, etc...).

`Envoyé`

: (à completer)

`Inconnu`

: Le statut du mail n'a pas pu être déterminé à partir des logs, probablement parce que quelque chose d'inhabituel s'est produit. Si la demande a été envoyée il y'a moins de 24h, le mieux est sans doute d'attendre pour voir comment les choses évoluent, il peut s'agir d'un problème temporaire, comme un serveur en cours de maintenance chez l'autorité destinatrice.

`Echoué`

: Cet état est normalement atteint seulement après 24h ou plus après l'envoi du message. Ceci signifie que le serveur
n'a pu correctement livrer le mail à son destinataire. Les causes sont nombreuses, mais la plus probable est que
l'adresse PRADA dans notre base de données est invalide.

#### Logs des serveurs mail

En cliquant sur l'état de livraison décrit ci-dessus (le `1` sur la capture d'écran ci-dessous), vous pouvez obtenir le
détail du statut, et pour les comptes admin, une copie des logs du serveur (la zone blanche marquée `2`).

Ces logs sont la meilleure source d'information pour comprendre ce qui pose problème avec le mail. Ils se lisent du haut
vers le bas. Les 2 ou 3 dernières lignes sont en général suffisantes.

![Statut d'envoi d'un message et détails des logs serveur](../../media/madada-screenshot-email-status-and-logs.png)

Quelques exemples:

- `status=sent (250 2.0.0 Ok: queued as 4L06nG4mMDzD1CJ)` sur l'avant-dernière ligne indique que le serveur mail
  destinataire a accepté le mail (`status=sent` pour les humains, code `250` pour les machines) et l'a mis dans sa file d'attente de traitement sous la référence `4L06nG4mMDzD1CJ`. Ici, tout s'est bien passé. Il est probable que l'explication du problème a été envoyée par un mail qui va apparaître en réponse dans la page de la demande.
- `status=deferred (connect to ...  Connection timed out)` ici le statut `deferred` signifie que notre serveur va
  retenter l'envoi plus tard, parce que l'erreur rencontrée n'est pas fatale. Cette erreur est donnée à la suite, ici
  `connect to... Connection timed out` signifie que le serveur destinataire n'a pas répondu, il est peut-être en
  maintenance, le réseau coupé, etc...
- `status=expired` signifie que notre serveur a essayé un certain nombre de fois d'envoyer le mail, mais il a abandonné
  après un nombre donné de tentatives. La ligne précédente donne probablement une idée de la dernière erreur rencontrée.
  En général, cette erreur se trouve à la fin de logs très longs (plusieurs dizaines, voire centaines de lignes, qui
  correspondent aux tentatives d'envoi, en général, la même erreur est répétée).
- `status=bounced (... <...>: Recipient address rejected: 511 "User Unknown" ...)` ici le status `bounced` signifie que
  le serveur destinataire a refusé le message et nous l'a "renvoyé", la raison suit: `Recipient address rejected... User Unknown`, le compte
  est inconnu. Voir "Adresse email non existante" ci-dessous.

Pas de panique si vous n'arrivez pas à interpreter les logs, ou que votre erreur n'est pas listée ici, c'est loin d'être
facile à lire, et cette liste est loin d'être exhaustive.

### Mail bloqué par un logiciel anti-spam côté administration (mailinblack)

Pour se protéger du spam, bon nombre d'administrations ont mis en place un système de validation humaine des emails
entrants (chez eux, donc "sortant" de notre point de vue). Un exemple de ces logiciels est "mailinblack". La première fois qu'ils reçoivent un mail d'une adresse
inconnue, ce qui est toujours le cas avec les mails magiques des demandes, ils envoient une réponse avec un lien à
cliquer. Celle-ci apparait sur la page de la demande concernée. Tant que le lien inclus dans ce message n'a pas été
ouvert et confirmé (généralement en cliquant sur un bouton ou en résolvant un captcha dans la page liée), le mail d'origine (donc la demande)
n'est pas délivré à son destinataire.

Si vous voyez de tels mails, il est important de les valider, sans quoi la demande n'est pas reçue par l'administration,
et ce, malgré la mention "Réceptionné" avec une coche verte en face de l'email sortant. Valider ces liens plusieurs fois
n'a pas d'impact négatif.

### Adresse email non existante, invalide ou obsolète

Le cas le plus fréquent est que l'adresse email dans notre base de données est invalide. Elle a pu changer, ou elle n'a
jamais été correcte. Dans tous les cas, il faut la mettre à jour.

Suivant la configuration du serveur mail destinataire, on aura soit une erreur au moment de l'envoi du type `User
Unknown` comme indiqué dans la section "Logs des serveurs mail" plus haut, ou le mail sera affiché comme "Réceptionné"
mais on aura une réponse, en général sous quelques secondes ou minutes, expliquant que l'adresse est invalide. Les deux
reviennent au même de notre point de vue.

### Autres problèmes de mail

D'autres problèmes plus complexes peuvent se présenter. Si la documentation ne suffit pas, contactez l'équipe technique
pour trouver une solution.
