# Support aux utilisatrices et utilisateurs de Ma Dada

En tant que bénévole dans l'équipe de support aux utilisateurs et utilisatrices de Ma Dada vous serez amené à répondre à toute sorte de demandes de la part de ces derniers. Vous trouverez ici une liste (non exhaustive) des demandes les plus fréquentes, ainsi que les procédures ou réponses types à apporter pour répondre à la demande.

!!!note

    Pour des raisons de neutralité et de praticité des usages, nous allons alterner l'usage du féminin, masculin, ou de mots regroupant les deux (comme par exemple utilisateurice qui signifie utilisatrices et utilisateurs).

## Demande Ma Dada non conforme (informations personnelles)

Ma Dada n'accepte pas les demandes d'accès à des documents personnels, comme par exemple une information au sujet d'une amende reçue, ou d'une déclaration d'impôts, ou tout autre document personnel. Il arrive assez souvent qu'un utilisateur ou une utilisatrice pense que Ma Dada est une plateforme officielle permettant d'obtenir ce genre d'information. On reçoit un certain nombre de courriels sur la boite mail contact de Ma Dada allant dans ce sens là. On remarque également des demandes publiques de documents personnels de ce type là, qu'il faut alors signaler, cacher, et faire remarquer à la personne que Ma Dada n'accepte pas ce type de demandes.

Voici un mail type à envoyer dans ce cas :

```
Madame, Monsieur,

Comme vous pouvez le lire sur notre page d'aide sur https://madada.fr/aide/demandes#data_protection le site Ma Dada est uniquement dédié aux demandes d'informations publiques. La plateforme ne peut donc pas être utilisée pour demander des informations privées sur vous ou sur une autre personne. Si vous souhaitez connaître les informations qu'une autorité publique détient sur vous, vous devriez faire une demande directement à l’autorité sans passer par le site Ma Dada.

Nous sommes donc dans l'impossibilité de traiter votre demande dans le cadre de Ma Dada. En espérant avoir pu tout de même vous aider.

L'équipe de Ma Dada
```

## L'autorité demande à l'utilisateurice de justifier sa demande

Il arrive que l'autorité demande à la personne effectuant une demande d'accès aux documents administratifs de justifier sa demande (le but, le pourquoi, son titre ou sa qualité, ... ) et conditionne sa réponse par l'obtention de ces informations. Ceci est non conforme à la loi CRPA.

Voici une réponse type à ajouter en commentaire de la demande et / ou à envoyer au demandeur ou demandeuse par mail dans ce cas. Ne pas hésiter à effectuer ce travail de manière pro-active et à veiller à ce que de tels comportements ne restent pas sans réponse de notre part, car ils pourraient intimider, induire de la peur ou de l'autocensure chez le demandeur.

```
Cher, Chère [nom-d'utilisateurice],

Dans votre demande ci-dessus \[ou lien vers la demande\], l'autorité administrative \[nom de l'autorité\], vous demande de justifier le but / la raison de votre demande d'accès aux documents administratifs.

Sachez que selon le CRPA (Code des Relations entre le Public et l'Administration), comme rappelé par ailleurs ici sur le site de la CADA, https://www.cada.fr/administration/modalites-de-communication :
« le demandeur n’a pas à faire état d’une qualité particulière, ni à préciser les motifs de sa demande ou à justifier d’un quelconque intérêt pour agir (20140351) ».

L'autorité administrative a donc obligation d'accéder à votre demande sans aucune précision ou justification de votre part. N'hésitez pas à lui répondre dans ce sens.

Cordialement,
L'équipe de MaDada
```

## Demande de coordonnées référent PRADA à une autorité

Lorsqu'on ne trouve pas de mail de contact d'une autorité (ni de mail PRADA, ni de mail générique, ni de mail dpo ou autre sur le site de l'autorité ou l'annuaire des PRADAs de la Cada), on a une dernière possibilité qui est d'envoyer une requête via les formulaires de contact disponibles sur le site web de l'autorité. Voici le mail type à envoyer :

```
Sujet : Demande de communication des coordonnées PRADA

Madame, Monsieur,

En application de la loi n° 78-575 du 17 juillet 1978 relative aux documents administratifs, nous souhaitons recevoir communication des coordonnées du service ou de la personne responsable de l'accès aux documents administratifs et des questions relatives à la réutilisation des informations publiques, autrement connu sous l'acronyme PRADA, auprès de votre administration.

En effet selon la CADA sur https://www.cada.fr/administration/la-nomination-dune-prada :
« L’article L.330-1 du CRPA dispose : « Les administrations mentionnées à l'article L. 300-2 sont tenues de désigner une personne responsable de l'accès aux documents et des questions relatives à la réutilisation des informations publiques, ... ».
La désignation de la personne responsable doit être portée à la connaissance des administrés selon les modalités les plus appropriées. En particulier, la publicité de cette nomination doit être faite, si l’administration dispose d’un site Internet, par la mise en ligne sur son site (articles R. 312-3 à R. 312-6). »

Sauf erreur de notre part, nous  n'avons pas pu trouver les coordonnées de votre PRADA sur votre site internet, ou sur l'annuaire public des PRADA. Pourriez-vous y remédier?

Veuillez agréer, Madame, Monsieur, l'expression de nos sentiments distingués.
L'équipe de madada.fr, la plateforme citoyenne de demandes d'accès aux documents administratifs gérée par Open Knowledge Foundation France.
```

## Demande de mise à jour de coordonnées référent PRADA à une autorité

Parfois l'annuaire des PRADAs de la CADA contient une adresse mail qui ne fonctionne plus, et on n'arrive pas à retrouver la nouvelle adresse. On peut alors envoyer une demande de mise à jour de cette adresse à l'autorité, via un mail de contact générique, ou le formulaire de contact sur le site web de l'autorité. Il peut être intéréssant de doubler cet envoi d'un courriel à la CADA pour les notifier de l'erreur sur leur annuaire.

```
Sujet : Demande de mise à jour de coordonnées PRADA erronées

Madame, Monsieur,

Sauf erreur de notre part, les coordonnées de la PRADA (Personne Responsable de l'Accès aux Documents Administratifs) auprès de votre administration ont changé ou ne correspondent plus à ce qui est indiqué sur l'annuaire des PRADA de la CADA / votre site internet : [lien]. Elles semblent également absentes de votre site internet.

En application de la loi n° 78-575 du 17 juillet 1978 relative aux documents administratifs, nous souhaitons recevoir communication des nouvelles coordonnées PRADA de votre administration. En outre nous vous invitons à en faire la publicité par leur mise en ligne, comme demandé par l'article L.330-1 du CRPA :
« Les administrations mentionnées à l'article L. 300-2 sont tenues de désigner une personne responsable de l'accès aux documents et des questions relatives à la réutilisation des informations publiques, ... ».
La désignation de la personne responsable doit être portée à la connaissance des administrés selon les modalités les plus appropriées. En particulier, la publicité de cette nomination doit être faite, si l’administration dispose d’un site Internet, par la mise en ligne sur son site (articles R. 312-3 à R. 312-6). »
Source : CADA sur https://www.cada.fr/administration/la-nomination-dune-prada :

Veuillez agréer, Madame, Monsieur, l'expression de nos sentiments distingués.

L'équipe de madada.fr, la plateforme citoyenne de demandes d'accès aux documents administratifs gérée par Open Knowledge Foundation France.
```

## Mail envoyé lorsqu'on cache une demande signalée

```
Cher/Chère [user],
Votre demande [demande] sur [lien demande] a été signalée comme demande contenant des informations personnelles et a à ce titre été étudiée par les modérateurs.
Nous considérons qu'il ne s'agit pas d'une demande d'accès à l'information appropriée sur notre site, justement car elle demande des informations personnelles. Sur notre plateforme les demandes sont publiques ainsi que leur réponses, or certaines demandes contenant des personnes nommées sont soit non valides soit communicables uniquement à la personne en question ou ses mandataires (voir https://www.service-public.fr/particuliers/vosdroits/F2467). Votre demande a donc été masquée pour les autres utilisateurs.

Vous serez toujours en mesure de la visualiser lorsque vous serez connecté au site. Merci de répondre à cet email si vous souhaitez échanger plus amplement sur cette décision. Merci également de ne pas utiliser notre plateforme pour des demandes personnelles à l'avenir. Pour en savoir plus sur le sujet je vous invite à lire notre FAQ ici : https://madada.fr/aide/a_propos#reporting

Sincèrement,
L'équipe de Ma Dada.
```

## Mail envoyé pour confirmer une demande d'ouverture de compte MaDada++

```
Bonjour [user],

C'est bon tu as maintenant un compte MaDada++, et donc l'accès à trois possibilités supplémentaires :
- les demandes en batch
- la possibilité de fixer un embargo sur tes demandes (de les rendre privées les demandes plus les réponses éventuelles le temps que tu le désire, mais le but de notre plateforme étant de mettre à disposition du public de l'open data pense à régulièrement faire le tour de tes demandes pour enlever les embargos là où tu penses que c'est possible
- un tableau de bord de suivi plus pratique

Tu peux lire plus la dessus sur
- ce post de notre blog : https://blog.madada.fr/articles/2020/02/20/madada++.html
- ou regarder la vidéo de notre webinaire ici : https://aperi.tube/w/xwYADx2vgXs2CKB2qfPoZu).

N'hésite pas si besoin à nous écrire sur contact@madada.fr, notre petite équipe de bénévoles te répondra aussi vite que possible. Tu peux aussi poser des questions sur notre forum ici : https://forum.madada.fr/, tu y trouveras plus de monde et d'autres personnes ayant des comptes Ma Dada++ qui pourront te répondre.

Ma Dada a besoin de votre soutien pour continuer à galoper en 2022. Pour faire un don c'est sur HelloAsso (https://www.helloasso.com/associations/open-knowledge-france/collectes/ma-dada-2022-pariez-sur-nous) ou sur LiberaPay (https://liberapay.com/madada/donate).

Merci et bienvenue parmi nous,
L'équipe Ma Dada.
```

## Maintenance du site

Seuls les personnes administratrices du site peuvent effectuer des opérations de maintenance.

L'interface d'administration est accessible via l'URL `/admin`. Seuls les personnes disposant des droits d'administration peuvent accéder à cette interface.

Les administratrices ou administrateurs du site peuvent :

- gérer les rôles des personnes utilisatrices
- classer les requêtes
- utiliser les liens "admin" qui s'affichent sur les requêtes, leurs réponses et les commentaires associés à celles-ci.

### Gestion de la zone tampon (holding pen)

Ma Dada stocke les messages entrants (les réponses) dans une zone tampon si leur adresse email ne peut pas être automatiquement associée à une requête.

Les deux raisons les plus courantes pour cela sont:

- la requête est close
- l'adresse email de destination a été incorrectement orthographiée

Lorsque ces cas surviennent, le message attend dans la zone tampon jusqu'à ce qu'une administratrice ou un administrateur l'attribue à la bonne requête ou le supprime.

Pour réaliser cette opération, il faut se rendre dans le menu d'administration intitulé [Summary](https://madada.fr/admin/) dans la section Holding pen accompagné du texte "Put misdelivered responses with the right request"

En cliquant sur ce message vous verrez apparaître la liste des messages nécessitant votre intervention. Cliquez sur l'un d'entre eux pour voir les détails.

Si le message n'appartient à aucune requête vous pouvez simplement le détruire. Pour cela il suffit de cliquer sur le bouton "destroy message" en bas de la page.

Lorsque vous inspectez un message, Ma Dada peut vous proposer une suggestion de rattachement à une requête existante. Inspectez cette requête. Si cette suggestion est correcte - le message entrant est bien une réponse en lien avec la demande - le champ `title_url` sera déjà pré-rempli dans le champ prévu. Cliquez sur le bouton "redeliver to another request" (rediriger vers une autre requête)"

S'il n'y a pas de suggestions, ou que celle proposée par Ma Dada est erronée, vous pouvez inspecter le champ d'adresse To: dans l'email brut et le contenu du message lui-même. C'est à vous d'essayer de deviner à quelle requête peut être associé cette réponse. Les adresses magiques générées par Ma Dada ont la forme :

    [INCOMING_EMAIL_PREFIX]request-[id]-[idhash]@[DOMAIN]

exemple :
dada+request-[id]-[idhash]@madada.fr

Sous cette forme, la première section de nombre après le terme request est l'identifiant de la requête. Vous pouvez naviguer et rechercher parmi les requêtes via l'interface d'administration en utilisant le menu "request" du bandeau d'administration. Lorsque vous avez identifié la bonne requête vous pouvez copier son identifiant ou son titre.

**:: Comment trouver l'identifiant ou le champ url_title d'une requête ?**

L'identifiant d'une requête est le nombre situé après le mot /show/ dans l'interface d'administration lorsque vous consultez la requête. Par exemple, si l'url est `/admin/demande/show/118`, alors son identifiant est 118. De manière similaire si vous souhaitez accéder à la page d'administration de cette requête, vous savez que son url sera `/admin/demande/show/118`.

Le champ `url_title` d'une requête est la partie située après le mot `/demande/` dans l'url du site Ma Dada lorsque vous consultez cette reqête. Dans l'url `/demande/liste_des_collectivites_locales`, le champ `url_title` est `liste_des_collectivites_locales`.

Une fois que vous avez identifié la requête à laquelle appartient le message entrant, retournez à la page Holding pen. Trouvez le bouton action associé au message entrant et collez l'identifiant ou le champ url_title dans le champ input. Cliquez sur le bouton "redeliver to another request".

Le message est désormais associé à la bonne requête. Il a quitté la zone tampon et est visible sur la page publique de la requête.

## Rejeter les pourriels qui arrivent dans la zone tampon

Ma Dada maintient une liste d'adresses pourriel. Chaque message entrant adressé à un email figurant dans cette liste sera automatiquement rejeté et n'apparaîtra pas dans l'interface d'administration et ne sera pas placé dans la zone tampon.

Si vous voyez des pourriels dans la zone tampon, vérifiez s'ils sont adressés à une adresse email spécifique. Si c'est le cas, cela signifie que cette adresse email est devenue une cible pour les spammeurs et que vous devez l'ajouter à la liste des adresses pourriels. Conséquemment Ma Dada rejetera automatiquement les messages provenant de cette adresse.

Une adresse email qui n'est pas associée à une requête (une dont les messages arrivent dans la zone tampon) devient une cible pour les spammeurs. Il existe plusieurs raisons expliquant l'existence de telles adresses invalides — par exemple, une erreur d'orthographe lors d'une réponse manuelle. D'expérience nous vous conseillons de rejeter les messages entrants associés à cette adresse lorsqu'elle a été ciblée de cette manière. Les emails légitimes arrivant dans la zone tampon ne se reproduisent généralement pas et la nature du cycle de vie des requêtes fait qu'elles ne sont généralement pas utilisées pour du spam avant d'être effectivement hors d'usage.

Pour ajouter une adresse email à la liste des adresses pourriels vous devez la copier depuis le message entrant et la coller dans la liste des adresses spam. Le moyen le plus simple de réaliser cette action est de cliquer sur le menu "summary" dans la barre d'administration et de cliquer sur "Put misdelivered responses with the right requests" afin de voir le contenu de la zone tampon.

A l'intérieur de la zone tampon, vous verrez une liste d'emails requièrant votre attention — cliquez sur le sujet du message afin de voir le message dans son intégralité. Copiez le champ To: email address, puis cliquez sur le lien "spam adresses" dans la rubrique Actions. Collez l'adresse email dans le champ input et cliquez sur le bouton "Paste the email address into the text input" et cliquez sur le bouton "Add Spam Address".

    Vous pouvez consulter la liste des adresses pourriels (c'est-à-dire, les adresses ciblées par les spammeurs) à n'importe quel moment en vous rendant à [cette url](https://madada.fr/admin/spam_addresses) .

Vous pouvez retirer une adresse de la liste en cliquant sur le bouton "remove" associé. Bien sûr, cela ne restaure pas les messages qui ont déjà été rejetés mais Ma Dada ne rejettera plus de messages associés à cette adresse.

Notez que si vous voyez un nombre important de messages pourriels dans la zone tampon vous devriez prendre des messures spéciales contre le spam.

## Cacher du texte d'une requête en utilisant les règles de censure <a name="admin-censor-rule"></a>

Des règles de censure peuvent être appliquées sur une requête ou une personne utilisatrice. Ces requêtes sont généralement motivés par le souhait de ne pas voir apparaître des coordonnées personnelles en lien avec une activité professionnelle et s'appuient sur le cadre réglementaire du RGPD. Dans [son avis du 08/07/2021](https://www.cada.fr/20213227), la Cada précise le cadre légal de diffusion de ces informations professionnelles tout en reconnaissant aux personnes concernées le droit de demander leur suppression.

Ces règles définissent des morceaux de texte à supprimer (soit dans la requête (et tous les fichiers associés par exemple les pièces jointes) soit dans l'ensemble des requêtes associées à la personne utilisatrice), et le texte de remplacement.

Dans les fichiers binaires, le texte de substitution sera toujours une série de caractères ‘x’ identiques en longueur au texte remplacé, de manière à préserver la longueur du fichier. La censure appliquée aux pièces jointes ne fonctionne pas toujours parfaitement, car il est difficile d'écrire des règles qui correspondent exactement au contenu du fichier. Il est donc nécessaire de vérifier les résultats après coup en vérifiant le contenu de la pièce jointe avec l'option "view as HTML".

Vous pouvez appliquer une règle de censure en utilisant une expression régulière en sélectionnant l'option "Is it regexp replacement?" dans l'interface d'administration. Le site utilise les expressions régulières de ruby. Vous pouvez tester votre expression sur [rubular](https://rubular.com/) avant de la mettre en place. Si vous n'avez aucune idée de ce que ceci signifie, demandez de l'aide à un.e autre admin.

Autrement, la règle de consure définie va simplement remplacer chaque occurence du texte saisi pour la substitution. De la même manière que les règles de censure basées sur du texte, les règles de censure basées sur des expressions régulières s'appliquent également aux fichiers attachés aux requêtes. En conséquence, une expression régulière trop générique risque d'avoir des conséquences inattendues si elle correspond également à une suite de caractères présente dans le fichier binaire. De plus, une expression régulière complexe ou trop généraliste peut affecter les performances du site lors de son exécution.

Il est donc conseillé de :

- se restreindre de les utiliser dans les cas qui peuvent être adressés autrement.
- écrire des expressions régulières simples ou bien spécifiques si possible.

Pour attacher une règle de censure à une requête, rendez-vous sur la page d'administration de la requête, et en bas de la page cliquez sur bouton "New censor rule (for this request only)". Sur la page suivante, saisissez le texte que vous voulez remplacer, par exemple ‘information privée’, le texte de substitution, par exemple ‘[information privée occultée]’, et un commentaire permettant aux autres personnes administratrices du site de savoir pourquoi vous avez caché cette information.

Pour attacher une règle de censure à une personne utilisatrice, qui sera appliquée à toutes les requêtes effectuées par cette personne, rendez-vous sur la page de cette personne dans le menu d'administration. Vous pouvez la trouver en utilisant le menu "Users" de l'administration et en navigant ou en cherchant la personne utilisatrice ou en suivant le lien "admin" associé à la personne utilisatrice dans l'interface publique lorsque vous êtes connecté en tant que personne administratrice.

Lorsque vous êtes sur la page d'administration de la personne utilisatrice, rendez-vous en bas de la page et cliquez sur le bouton 'New censor rule'. Sur la page suivante, entrez le texte que vous voulez substituer par exemple 'mon vrai nom est Sydnay Poitier', le texte de substitution par exemple. ‘[information personnelle occultée]’, et un commentaire permettant aux autres personnes administratrices de comprendre pourquoi vous avez modifié cette information.
