# Templates de saisine CADA

Les templates dans ce dossier ont pour certain une variante `+cada` (qui précède l'extension `.erb` dans le nom de fichier.
Ces templates sont seulement utilisés pour générer le fichier de saisine de la CADA, et ne sont donc pas visibles en html sur le site, seulement dans un PDF téléchargé.
