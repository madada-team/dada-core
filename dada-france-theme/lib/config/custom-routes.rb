# -*- encoding : utf-8 -*-

# Here you can override or add to the pages in the core website

Rails.application.routes.draw do
  # brand new controller example
  # get '/mycontroller' => 'general#mycontroller'
  # Additional help page example
  # get '/help/help_out' => 'help#help_out'

  get '/forum' => redirect('https://forum.madada.fr'),
      :as => :forum

  get '/stats' => redirect('https://stats.madada.fr/public/dashboard/88dfe578-ca74-4b21-8c46-65259083ce47'),
      :as => :stats

  get '/madada_blog' => redirect('https://blog.madada.fr/'),
      :as => :madada_blog

  get '/dons' => redirect(AlaveteliConfiguration.donation_url),
      :as => :madada_donations

  get '/a_propos' => redirect('https://doc.madada.fr/a_propos'),
      :as => :a_propos

  # test a short url for a pre-filled request with no public body pre-selected
  # The url redirect is obtained by URL encoding the actual text of the request
  get '/campagne/eins' => redirect('/selectionner_autorite?request_title=%C3%89tude%20d%27impact%20nuisances%20sonores&request_body=Madame%2C%20Monsieur%2C%0A%0AAu%20titre%20du%20droit%20d%E2%80%99acc%C3%A8s%20aux%20documents%20administratifs%2C%20tel%20que%20pr%C3%A9vu%20notamment%20par%20le%20Livre%20III%20du%20Code%20des%20relations%20entre%20le%20public%20et%20l%E2%80%99administration%2C%20je%20sollicite%20aupr%C3%A8s%20de%20vous%20la%20communication%20de%20l%E2%80%99%C3%A9tude%20d%E2%80%99impact%20de%20nuisances%20sonores%20%28EINS%29%20concernant%20%5Bmettre%20ici%20le%20nom%20de%20l%E2%80%99%C3%A9tablissement%20concern%C3%A9%5D%2C%20situ%C3%A9%20%C3%A0%20l%E2%80%99adresse%20%5Bcompl%C3%A9ter%5D%2E%0A%0ACette%20%C3%A9tude%20d%E2%80%99impact%2C%20pr%C3%A9vue%20par%20l%E2%80%99article%20R571%2D27%20du%20Code%20de%20l%E2%80%99environnement%2C%20doit%20%C3%AAtre%20%C3%A9tablie%20par%20les%20responsables%20de%20lieux%20ouverts%20au%20public%20ou%20recevant%20du%20public%2C%20clos%20ou%20ouvert%2C%20accueillant%20%C3%A0%20titre%20habituel%20des%20activit%C3%A9s%20de%20diffusion%20de%20sons%20amplifi%C3%A9s%2C%20afin%20de%20pr%C3%A9venir%20les%20nuisances%20sonores%20de%20nature%20%C3%A0%20porter%20atteinte%20%C3%A0%20la%20tranquillit%C3%A9%20ou%20%C3%A0%20la%20sant%C3%A9%20du%20voisinage%2E%0A%0ALa%20Commission%20d%E2%80%99acc%C3%A8s%20aux%20documents%20administratifs%20%28CADA%29%20juge%20de%20mani%C3%A8re%20constante%20que%20ce%20document%20est%20communicable%20%C3%A0%20toute%20personne%20qui%20le%20demande%20en%20application%20de%20l%E2%80%99article%20L311%2D1%20du%20Code%20des%20relations%20entre%20le%20public%20et%20l%E2%80%99administration%20et%20des%20articles%20L124%2D1%20et%20suivants%20du%20Code%20de%20l%E2%80%99environnement%2E%0A%0A%E2%80%9CL%E2%80%99autorit%C3%A9%20publique%20ne%20peut%20rejeter%20une%20demande%20portant%20sur%20une%20information%20relative%20%C3%A0%20des%20%C2%AB%E2%80%AF%C3%A9missions%20de%20substances%20dans%20l%E2%80%99environnement%E2%80%AF%C2%BB%20que%20dans%20le%20cas%20o%C3%B9%20sa%20communication%20porterait%20atteinte%20%C3%A0%20la%20conduite%20de%20la%20politique%20ext%C3%A9rieure%20de%20la%20France%2C%20%C3%A0%20la%20s%C3%A9curit%C3%A9%20publique%20ou%20%C3%A0%20la%20d%C3%A9fense%20nationale%2C%20ou%20encore%20au%20d%C3%A9roulement%20des%20proc%C3%A9dures%20juridictionnelles%2C%20%C3%A0%20la%20recherche%20d%E2%80%99infractions%20pouvant%20donner%20lieu%20%C3%A0%20des%20sanctions%20p%C3%A9nales%20ou%20enfin%20%C3%A0%20des%20droits%20de%20propri%C3%A9t%C3%A9%20intellectuelle%2E%20Ces%20dispositions%20font%20en%20revanche%20obstacle%20%C3%A0%20ce%20que%20l%E2%80%99autorit%C3%A9%20administrative%20en%20refuse%20la%20communication%20au%20motif%20qu%E2%80%99elles%20comporteraient%20des%20mentions%20couvertes%20par%20le%20secret%20des%20affaires%E2%80%9D%20a%20par%20exemple%20soulign%C3%A9%20la%20CADA%20dans%20son%20avis%2020184501%20du%2018%20avril%202019%2E%0A%0AJe%20vous%20prie%20de%20bien%20vouloir%20m%E2%80%99envoyer%20ce%20document%20sous%20forme%20%C3%A9lectronique%2C%20dans%20un%20standard%20ouvert%2C%20ais%C3%A9ment%20r%C3%A9utilisable%20et%20exploitable%20par%20un%20syst%C3%A8me%20de%20traitement%20automatis%C3%A9%2C%20comme%20le%20pr%C3%A9voit%20l%E2%80%99article%20L300%2D4%20du%20Code%20des%20relations%20entre%20le%20public%20et%20l%E2%80%99administration%2E%0A%0AVeuillez%20agr%C3%A9er%2C%20Madame%2C%20Monsieur%2C%20l%27expression%20de%20mes%20sentiments%20distingu%C3%A9s%2E%0A'),
      :as => :campagne_eins

  match '/api/v2/tags.json' => 'api#list_tags',
        :as => :api_list_tags,
        :via => :get

  match '/demande/:url_title/tableur_saisine' => 'request#download_batch_saisine_spreadsheet',
        :as => :download_batch_saisine_spreadsheet,
        :via => :get

  match '/request/:url_title/download_saisine' => 'request#download_entire_request_saisine',
        :as => :download_entire_request_saisine,
        :via => :get

  match '/demande/:url_title/saisir_la_cada' => 'request#send_cada_appeal',
        :as => :send_cada_appeal,
        :via => %i[get patch]

  match '/alaveteli_pro/info_requests/:url_title/saisir_la_cada' => 'request#send_cada_appeal',
        :as => :send_cada_appeal_pro,
        :via => %i[get patch]

  match '/documents' => 'documents#list',
        :as => :documents_list,
        :via => :get

  # HELP / AIDE
  match '/aide/a_propos' => redirect('https://doc.madada.fr/'),
        :as => :help_about,
        :via => :get
  match '/aide/alaveteli' => redirect('https://doc.madada.fr/'),
        :as => :help_alaveteli,
        :via => :get
  match '/aide/prada' => redirect('https://doc.madada.fr/prada'),
        :as => :help_officers,
        :via => :get
  match '/aide/demandes' => redirect('https://doc.madada.fr/'),
        :as => :help_requesting,
        :via => :get
  match '/aide/confidentialite' => redirect('https://doc.madada.fr/usagers/confidentialite'),
        :as => :help_privacy,
        :via => :get
  match '/aide/credits' => redirect('https://doc.madada.fr/usagers/confidentialite'),
        :as => :help_credits,
        :via => :get
  match '/aide/cada' => redirect('https://doc.madada.fr/prada'),
        :as => :help_cada,
        :via => :get
end
