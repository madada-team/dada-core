# Add a callback - to be executed before each request in development,
# and at startup in production - to patch existing app classes.
# Doing so in init/environment.rb wouldn't work in development, since
# classes are reloaded, but initialization is not run each time.
# See http://stackoverflow.com/questions/7072758/plugin-not-reloading-in-development-mode
#
require 'digest'
# require various files just to make it a bit easier to organise code
require 'reminders'
require 'sparql/client'

Rails.configuration.to_prepare do
  # Example of adding a default text to each message
  # OutgoingMessage.class_eval do
  #   # Add intro paragraph to new request template
  #   def default_letter
  #     # this line allows the default_letter text added by this
  #     # method to be replaced by the value supplied by the API
  #     # e.g. http://demo.alaveteli.org/new/tgq?default_letter=this+is+a+test
  #     return @default_letter if @default_letter
  #     return nil if self.message_type == 'followup'
  #     "If you uncomment this line, this text will appear as default text in every message"
  #   end
  # end

  AlaveteliConfiguration.class_eval do
    # Allow additional config keys to be defined. This is a bit clunky
    # but allows using the regular config file for this, and our ansible
    # system to pass config in a secure manner
    def self.dadabot_email_address
      MySociety::Config.get('DADABOT_EMAIL_ADDRESS', 'bot@example.com')
    end

    def self.cada_email_address
      MySociety::Config.get('CADA_EMAIL_ADDRESS', 'cada@example.com')
    end
  end

  ApplicationHelper.class_eval do
    # These are used in search result listings for instance. Overridden here
    # to hide user names
    def event_description(event)
      body_link = public_body_link_absolute(event.info_request.public_body)
      user_link = request_user_link_absolute(event.info_request)
      date = simple_date(event.created_at)
      case event.event_type
      # cases below use an ugly hack by trying to translate a non existing sentence
      # this returns the same. Somehow, formatting the sentence directly via interpolation
      # breaks all links.
      when 'sent'
        _('Demande envoyée à {{public_body_name}} le {{date}}.',
          public_body_name: body_link,
          info_request_user: user_link,
          date: date)
      when 'followup_sent'
        case event.calculated_state
        when 'internal_review'
          _('Recours amiable envoyé à {{public_body_name}} le {{date}}.',
            public_body_name: body_link,
            info_request_user: user_link,
            date: date)
        when 'waiting_response'
          _('Clarification envoyée à {{public_body_name}} le {{date}}.',
            public_body_name: body_link,
            info_request_user: user_link,
            date: date)
        else
          _('Message de suivi envoyé à {{public_body_name}} le {{date}}.',
            public_body_name: body_link,
            info_request_user: user_link,
            date: date)
        end
      when 'response'
        _('Réponse de {{public_body_name}} le {{date}}.',
          public_body_name: body_link,
          info_request_user: user_link,
          date: date)
      when 'comment'
        _('Demande envoyée à {{public_body_name}}. Commentée le {{date}}.',
          public_body_name: body_link,
          info_request_user: user_link,
          event_comment_user: user_link_absolute(event.comment.user),
          date: date)
      end
    end
  end

  InfoRequest.class_eval do
    def make_zip_cache_path_for_saisine(user)
      # This method is only used for the saisine CADA PDF doc.
      # The filename needs to be different from the "regular" PDF as the content
      # of this one is different. This prevents the cache from returning the wrong
      # file, should users request both files.
      # The zip file varies depending on user because it can include different
      # messages depending on whether the user can access hidden or
      # requester_only messages. We name it appropriately, so that every user
      # with the right permissions gets a file with only the right things in.
      cache_file_dir = File.join(InfoRequest.download_zip_dir,
                                 'download',
                                 request_dirs,
                                 last_update_hash)
      cache_file_suffix = zip_cache_file_suffix(user)
      File.join(cache_file_dir, "saisine_#{url_title}#{cache_file_suffix}.zip")
    end

    # assemble an openDocument spreadsheet containing the following columns for each
    # request that was part of the batch the request is part of:
    # - name of the targeted entity
    # - request email address
    # - date at which the request was last sent
    #   (we assume that last time it was sent was successful)
    # - date of explicit rejection by the authority (skipped for now)
    # Do not cache this file, as the variety of events that would lead to
    # clear the cache is too large.
    # It is not meant to be downloaded more than once in reality anyway.
    def create_saisine_spreadsheet(ids_to_include = nil)
      data = []
      unless info_request_batch.nil?
        if ids_to_include.nil?
          # exclude the current request id so we can prepend it further down
          reqs = info_request_batch&.info_requests.where.not(id: id)
        else
          # prepend the id of the current request as the html form does not include it
          # when the checkbox is disabled
          reqs = info_request_batch&.info_requests.where("id IN (?)", ids_to_include)
        end
        # make sure the current request's data is the first row in the spreadsheet
        data = reqs.map(&:saisine_spreadsheet_row).prepend(saisine_spreadsheet_row)
      end
      headers = ["Nom de l'administration",
                 'Adresse électronique',
                 'Date de réception de la demande de communication',
                 "Date de notification du refus explicite par l'administration (le cas échéant)"]
      SpreadsheetArchitect.to_ods(headers: headers, data: data)
    end

    # return a record for the CADA appeal summary spreadsheet
    # used for batch requests
    def saisine_spreadsheet_row
      [
        public_body.name,
        public_body.request_email,
        date_initial_request_last_sent_at,
        (last_public_response_at&.strftime('%d-%m-%Y') if described_state == 'rejected')
      ]
    end

    def mark_appeal_sent
      # update the request state and save an event
      log_event(
        'status_update',
        user_id: user.id,
        old_described_state: described_state,
        described_state: 'saisine_cada',
        message: "Saisine envoyée à la CADA"
      )
      # change status to error in delivery
      # updates the state, triggers email notification "someone updated your request"
      set_described_state(
        'saisine_cada',
        user,
        "Saisine envoyée à la CADA"
      )
    end

    # override the base method to leave "awaiting_description" unchanged
    # when receiving a proof of delivery from the CADA after an appeal
    def create_response!(_email, raw_email_data, rejected_reason = nil)
      incoming_message = incoming_messages.build

      # To avoid a deadlock when simultaneously dealing with two
      # incoming emails that refer to the same InfoRequest, we
      # lock the row for update.
      with_lock do
        # TODO: These are very tightly coupled
        raw_email = RawEmail.new
        incoming_message.raw_email = raw_email
        incoming_message.save!
        raw_email.data = raw_email_data
        raw_email.save!

        # this is what is changed compared to upstream
        unless described_state == 'user_withdrawn' ||
               (described_state == 'saisine_cada' &&
                incoming_message.from_email == AlaveteliConfiguration.cada_email_address &&
                incoming_message.subject == "CADA - Accusé de réception d'un courriel")
          self.awaiting_description = true
        end

        params = { incoming_message_id: incoming_message.id }
        params[:rejected_reason] = rejected_reason.to_s if rejected_reason
        log_event('response', params)

        save!
      end

      # for the "waiting_classification" index
      reindex_request_events

      incoming_message
    end

    # generate a hard to guess tag that the cron job can use to decide which requests
    # to send a CADA appeal notification email for. The tag is of the form
    # appeal_notification_pending:<hash>
    def calculate_appeal_notification_email_tag
      # refer to https://groups.google.com/g/alaveteli-dev/c/HZkeahWuv-U
      # for more context
      tag_name = "appeal_notification_pending"
      # these 3 values are *very* unlikely to change within the few minutes between
      # the user appealing and the cron job run
      v = "#{id}#{user_id}#{public_body_id}"
      md5 = Digest::MD5.new
      md5.update(v)
      "#{tag_name}:#{md5.hexdigest}"
    end

    def self.search_by_tag(tag_as_string, limit=5)
      # FIXME: does the same job as find_by_tag which does NOT
      # work on InfoRequest for some reason. Figure out why
      join_sql = <<-EOF.strip_heredoc.squish
      LEFT JOIN has_tag_string_tags
      ON has_tag_string_tags.model_type = 'InfoRequest'
      AND has_tag_string_tags.model_id = info_requests.id
      EOF

      search =
        joins(join_sql).
        where(has_tag_string_tags: { name: tag_as_string, model_type: to_s }).
        references(:has_tag_string_tags)

      search.distinct.limit(limit)
    end

    def self.featured_requests(max_count=5)
      # return an array of custom objects which hold info
      # about featured requests
      request_list = InfoRequest.search_by_tag("featured", limit=max_count).filter {|r| !r.embargoed?}

      req_list = request_list.select {|r| r.all_notes.length > 0 }.map do |r|
        m = /^:text:(?<text>.*)\n:image:(?<image>.*)$/.match(r.all_notes.select {|n| n.body.starts_with?(':text:')}.first.body)
        {
          featured_url: r.url_title,
          featured_text: m[:text],
          featured_image: m[:image]
        }
      end
      # get rid of requests which where tagged 'featured' but had no
      # usable note attached.
      req_list.compact
    end
  end

  # custom state transition labels as shown in the Actions / update status box
  InfoRequest::State::Transitions.class_eval do
    def self.owner_waiting_response_transition_label(_opts = {})
      _("Je suis encore <strong>en attente</strong> d'une réponse " \
        "<small>(peut-être avez-vous reçu un accusé de réception...)</small>")
    end

    def self.owner_not_held_transition_label(_opts = {})
      _("L'autorité <strong>ne détient pas</strong> les documents " \
        "<small>(peut-être qu'ils indiquent qui les détient, ou qu'ils n'existent pas)</small>")
    end

    def self.owner_rejected_transition_label(_opts = {})
      _("My request has been <strong>refused</strong>")
    end

    def self.owner_partially_successful_transition_label(_opts = {})
      # keep the translation function to handle html tags
      _("J'ai reçu <strong>une partie</strong> des documents demandés")
    end

    def self.owner_successful_transition_label(_opts = {})
      _("J'ai reçu <strong>tous</strong> les documents demandés")
    end

    def self.owner_waiting_clarification_transition_label(_opts = {})
      "J'ai été invité.e à clarifier ma demande"
    end

    def self.owner_gone_postal_transition_label(_opts = {})
      _("They are going to reply <strong>by postal mail</strong>")
    end

    def self.owner_internal_review_transition_label(opts = {})
      if opts.fetch(:in_internal_review, false) ||
         opts.fetch(:internal_review_requested, false)
        _("I'm still <strong>waiting</strong> for the internal review")
      else
        _("I'm waiting for an <strong>internal review</strong> response")
      end
    end

    def self.owner_error_message_transition_label(_opts = {})
      _("I've received an <strong>error message</strong>")
    end

    def self.owner_requires_admin_transition_label(_opts = {})
      _("This request <strong>requires administrator attention</strong>")
    end

    def self.owner_user_withdrawn_transition_label(_opts = {})
      _("I would like to <strong>withdraw this request</strong>")
    end

    def self.other_user_waiting_response_transition_label(_opts = {})
      _("La demande est encore <strong>en attente</strong> d'une réponse " \
        "<small>(peut-être est-ce un accusé de réception...)</small>")
    end

    def self.other_user_not_held_transition_label(_opts = {})
      _("L'autorité <strong>ne détient pas</strong> les documents " \
        "<small>(peut-être qu'ils indiquent qui les détient, ou qu'ils n'existent pas)</small>")
    end

    def self.other_user_rejected_transition_label(_opts = {})
      _("La demande a été <strong>refusée</strong>")
    end

    def self.other_user_partially_successful_transition_label(_opts = {})
      _("<strong>Une partie</strong> des documents demandés ont été reçus")
    end

    def self.other_user_successful_transition_label(_opts = {})
      _("<strong>Tous</strong> les documents demandés ont été reçus")
    end

    def self.other_user_waiting_clarification_transition_label(_opts = {})
      _("<strong>Clarification</strong> has been requested")
    end

    def self.other_user_gone_postal_transition_label(_opts = {})
      _("A response will be sent <strong>by postal mail</strong>")
    end
  end

  IncomingMessage.class_eval do
    # detect some common error messages (the ones sent by SMTP servers)
    def error_message?
      sender_email = from_email.downcase
      sender_email.start_with?('postmaster@') ||
        sender_email.start_with?('mailer-daemon') ||
        (sender_email.start_with?('mdaemon@') &&
           subject.downcase.include?('utilisateur inconnu'))
    end

    def has_tag_value?(name, value)
      has_tag?(name) &&
        get_tag_values(name).include?(value)
    end

    def tagged_proof_of_delivery?
      has_tag_value?('message', 'accuse_reception')
    end

    # vaarious checks to identify delivery receipts. Each public body has
    # some variation of its own, this tries to group them in a somewhat
    # manageable way.
    def cada_delivery_receipt?
      from_email == AlaveteliConfiguration.cada_email_address &&
        subject == "CADA - Accusé de réception d'un courriel"
    end

    def ministry_delivery_receipt?
      # strip newlines so we can match long strings despite text
      # folding in some email clients
      unless info_request.public_body.has_tag?('ministere') &&
             from_email == info_request.public_body.request_email
        return false
      end

      # the folded logic is very biased towards emails managed in English.
      # for email software running in French, it is useless more often than not,
      # so we look only at the beginning of messages to avoid false positives due
      # to replies containing the original message/reply.
      full_main_body_text = get_main_body_text_folded[0..500].gsub("\n", '')
      # search string broken down to please linter
      mefr1 = /La personne responsable de l’accès aux documents administratifs /
      mefr2 = /du ministère de l’Economie, des Finances et de la Souveraineté /
      mefr3 = /Industrielle et Numérique a reçu le .{6,30} votre demande /
      mefr4 = /d’accès à un ou plusieurs documents administratifs./
      mefr_search_string = /#{mefr1}#{mefr2}#{mefr3}#{mefr4}/

      !(full_main_body_text =~ /Nous (en )?accusons bonne réception/i).nil? ||
        !full_main_body_text.index(mefr_search_string).nil? ||
        !(full_main_body_text =~ /J’accuse réception de votre (demande|courriel) /i).nil? ||
        !(full_main_body_text =~ /Nous accusons réception de (la|votre) demande/i).nil?
    end

    def spf_delivery_receipt?
      if from_email.end_with?('dgfip.finances.gouv.fr')
        full_main_body_text = get_main_body_text_folded.gsub("\n", '')
        s1 = /Nous vous informons que votre demande a bien été prise en compte. /
        s2 = /Nous allons y répondre dans les meilleurs délais, merci de ne pas /
        s3 = /réitérer votre demande./
        search_string = /#{s1}#{s2}#{s3}/

        full_main_body_text.start_with?(search_string)
      else
        false
      end
    end

    # Add a class method which will select recently received
    # messages, and apply some heuristics to automatically
    # classify the ones that are easy.
    def self.filter_new_emails(how_long_ago = 1.hour)
      # use a 1 hour time window to make sure we don't miss messages, for instance
      # during deployments, which can prevent the rake task from running.
      # We can safely run this code twice on a single message as it won't have any
      # weird side effects. This filter is expected to run every 5 minutes.
      n_minutes_ago = Time.zone.now - how_long_ago

      dadabot_user = User.find_by_email(AlaveteliConfiguration.dadabot_email_address)
      notif_tag_string = 'captcha:notification'
      file_transfer_tag_string = 'message:transfert_de_fichier'
      delivery_receipt_tag_string = 'message:accuse_reception'

      # reduce the default batch size from 1000 to 100 to limit
      # memory errors while processing large batches
      where(
        ['incoming_messages.created_at > ?', n_minutes_ago]
      ).joins(
        :info_request
      ).where(
        'info_requests.awaiting_description=true'
      ).find_each(batch_size: 100) do |email|
        sender_email = email.from_email.downcase

        # email delivery error messages
        if email.error_message?

          ir = email.info_request
          logger.info "Marking request #{ir.id} as having an email_error"
          # log an event that is visible in the request's admin page
          ir.log_event(
            'status_update',
            user_id: dadabot_user.id,
            old_described_state: ir.described_state,
            described_state: 'error_message',
            message: "[DadaBot] Message d'erreur reçu du serveur destinataire"
          )
          # change status to error in delivery
          # updates the state, triggers email notification "someone updated your request"
          ir.set_described_state(
            'error_message',
            dadabot_user,
            "Message d'erreur reçu du serveur destinataire"
          )
          # skip the rest of the checks, as an email should only be one of them
          next
        end

        # captcha notifications
        # After the email is tagged, the admin interface will show corresponding
        # requests under a specific section.
        if (sender_email.include?('@mailinblack') ||
           sender_email.include?('@invitations.mailinblack.com') ||
           sender_email.start_with?('humail@') ||
           sender_email =~ /@.*.iantispam.fr/ ||
           sender_email =~ /@antispam.*.xefi.fr/) &&
           !email.has_tag_value?('captcha', 'notification')
          logger.info "Tagging incoming_message #{email.id} with #{notif_tag_string}"
          email.add_tag_if_not_already_present(notif_tag_string)
          email.info_request.log_event(
            'edit',
            user_id: dadabot_user.id,
            message: 'Notification captcha antispam identifiée'
          )
          next
        end

        # identify delivery receipts
        if !email.tagged_proof_of_delivery? &&
           (email.cada_delivery_receipt? ||
          email.ministry_delivery_receipt? ||
          email.spf_delivery_receipt?)
          logger.info "Tagging incoming_message #{email.id} with #{delivery_receipt_tag_string}"
          email.add_tag_if_not_already_present(delivery_receipt_tag_string)
          email.info_request.log_event(
            'edit',
            user_id: dadabot_user.id,
            message: "Accusé de réception identifié dans le message id: #{email.id}"
          )
          next
        end

        # file transfer links, from various providers.
        full_main_body_text = email.get_main_body_text_internal
        if !email.has_tag_value?('message', 'transfert_de_fichier') &&
           (sender_email.include?('noreply@wetransfer.com') ||
           full_main_body_text.include?('https://wetransfer.com/download') ||
           full_main_body_text.include?('https://wetransfer.com/downloads') ||
           full_main_body_text.include?('https://we.tl') ||
           full_main_body_text.include?('https://www.grosfichiers.com') ||
           full_main_body_text.include?('https://transfert.dnpm.fr') ||
           full_main_body_text.include?('https://cloud.saintnazaire.fr') ||
           full_main_body_text.include?('https://echanges.grandlyon.com/share/') ||
           full_main_body_text.include?('https://adoc.ille-et-vilaine.fr') ||
           full_main_body_text.include?('https://transferts.indre.fr') ||
           full_main_body_text.include?('partage-de-fichiers.brest-metropole.fr') ||
           full_main_body_text.include?('https://equipes.villeurbanne.fr/nextcloud/') ||
           full_main_body_text.include?('francetransfert.numerique.gouv.fr') ||
           full_main_body_text.include?('partage.strasbourg.eu/share-access') ||
           full_main_body_text.include?('melanissimo-ng.din.developpement-durable.gouv.fr') ||
           full_main_body_text.include?('sofie.finances.gouv.fr') ||
           full_main_body_text.include?('echange.cnil.fr') ||
           full_main_body_text.include?('pose.cg60.fr') ||
           full_main_body_text.include?('transfert.cg37.fr') ||
           full_main_body_text.include?('https://www.swisstransfer.com') ||
           full_main_body_text.include?('fromsmash.com') ||
           full_main_body_text.include?('https://filesender.renater.fr') ||
           full_main_body_text.include?('https://lft.orange.fr/') ||
           full_main_body_text.include?('transfert.scnbdx.fr/w') ||
           full_main_body_text.include?('https://www.nextsend.com/download') ||
           full_main_body_text.include?('https://transfert.cotesdarmor.fr'))
          logger.info "Tagging incoming_message #{email.id} with #{file_transfer_tag_string}"
          email.add_tag_if_not_already_present(file_transfer_tag_string)
          email.info_request.log_event(
            'edit',
            user_id: dadabot_user.id,
            message: 'Notification de transfert de fichier identifiée'
          )
        end
      end
    end

    def custom_text_for_admins
      text = get_main_body_text_internal
      # Strip the uudecode parts from main text
      # - this also effectively does a .dup as well, so text mods don't alter original
      text = text.split(/^begin.+^`\n^end\n/m).join(' ')

      if text.size > 1_000_000 # 1 MB ish
        raise 'main body text more than 1 MB, need to implement clipping like for attachment '\
              'text, or there is some other MIME decoding problem or similar'
      end

      # apply masks for this message
      apply_masks(text, 'text/html', false)
    end

    def custom_text_folded_for_admins
      text = custom_text_for_admins
      # Remove existing quoted sections
      folded_quoted_text = remove_lotus_quoting(text, 'FOLDED_QUOTED_SECTION')
      folded_quoted_text = IncomingMessage.remove_quoted_sections(folded_quoted_text,
                                                                  'FOLDED_QUOTED_SECTION')
      folded_quoted_text.delete("\0")
    end

    def custom_text_unfolded_for_admins
      text = custom_text_for_admins
      text.delete("\0")
    end

    def get_main_body_text_folded(hide_all_emails = true)
      if hide_all_emails
        # same behaviour as upstream alaveteli
        _cache_main_body_text if cached_main_body_text_folded.nil?
        cached_main_body_text_folded
      else
        # bypass caching mechanism for admins (and possibly requester)
        custom_text_folded_for_admins
      end
    end

    def get_main_body_text_unfolded(hide_all_emails = true)
      if hide_all_emails
        # same behaviour as upstream alaveteli
        _cache_main_body_text if cached_main_body_text_unfolded.nil?
        cached_main_body_text_unfolded
      else
        # bypass caching mechanism for admins (and possibly requester)
        custom_text_unfolded_for_admins
      end
    end

    def apply_masks(text, content_type, hide_all_emails = true)
      # override the standard method to skip censoring if not required
      mask_options = { censor_rules: info_request.applicable_censor_rules,
                       masks: info_request.masks,
                       hide_all_emails: hide_all_emails }
      AlaveteliTextMasker.apply_masks(text, content_type, mask_options)
    end

    def get_body_for_html_display(collapse_quoted_sections = true, hide_all_emails = true)
      # text content of emails is cached once for all users. We bypass the cache for
      # admins, to make sure we don't accidentally cache an uncensored version of the
      # text.
      # This overridden method just adds hide_all_emails argument to the 2 calls below
      # Find the body text and remove emails for privacy/anti-spam reasons
      text = get_main_body_text_unfolded(hide_all_emails)
      folded_quoted_text = get_main_body_text_folded(hide_all_emails)

      # Remove quoted sections, adding HTML. TODO: The FOLDED_QUOTED_SECTION is
      # a nasty hack so we can escape other HTML before adding the unfold
      # links, without escaping them. Rather than using some proper parser
      # making a tree structure (I don't know of one that is to hand, that
      # works well in this kind of situation, such as with regexps).
      text = folded_quoted_text if collapse_quoted_sections
      text = MySociety::Format.simplify_angle_bracketed_urls(text)
      text = CGI.escapeHTML(text)
      text = MySociety::Format.make_clickable(text, contract: 1)

      # add a helpful link to email addresses and mobile numbers removed
      # by apply_masks
      email_pattern = Regexp.escape(_('email address'))
      mobile_pattern = Regexp.escape(_('mobile number'))
      text.gsub!(/\[(#{email_pattern}|#{mobile_pattern})\]/,
                 '[<a href="/help/officers#mobiles">\1</a>]')

      if collapse_quoted_sections
        text = text.gsub(/(\s*FOLDED_QUOTED_SECTION\s*)+/m, 'FOLDED_QUOTED_SECTION')
        text.strip!
        # if there is nothing but quoted stuff, then show the subject
        if text == 'FOLDED_QUOTED_SECTION'
          text = '[Subject only] ' + CGI.escapeHTML(subject || '') + text
        end
        # and display link for quoted stuff
        text = text.gsub('FOLDED_QUOTED_SECTION',
                         "\n\n" + '<span class="unfold_link"><a href="?unfold=1#incoming-' +
                         id.to_s + '">' + _('show quoted sections') + '</a></span>' + "\n\n")
      elsif folded_quoted_text.include?('FOLDED_QUOTED_SECTION')
        text = text + "\n\n" + '<span class="unfold_link"><a href="?#incoming-' + id.to_s + '">' +
               _('hide quoted sections') + '</a></span>'
      end
      text.strip!

      text = ActionController::Base.helpers.simple_format(text)
      text.html_safe
    end
  end

  InfoRequestHelper.class_eval do
    # Override the default text in the red banner in the header of a request view
    # to replace internal review with cada appeal instead.
    def status_text_waiting_response_very_overdue(info_request, _opts = {})
      str = _('Response to this request is <strong>long overdue</strong>.')
      str += ' '
      str += if info_request.public_body.not_subject_to_law?
               _('Although not legally required to do so, we would have ' \
                 'expected {{public_body_link}} to have responded by now',
                 public_body_link: public_body_link(info_request.public_body))
             else
               "Selon la loi, l'administration aurait déjà dû répondre"
             end
      str += ' '

      unless info_request.is_external?
        str += ' '
        str += 'Si vous avez envoyé la demande initiale, '
        str += 'et si les délais le permettent encore, vous pouvez '
        str += link_to 'saisir la CADA',
                       'https://doc.madada.fr/usagers/procedure-de-demande/#recours-administratif-saisine-cada'
        # str += link_to 'envoyer une saisine à la CADA',
        # new_request_followup_path(request_id: info_request.id) +
        # '?internal_review=1'
        str += '.'
      end

      str
    end

    # Override the whole method here as the translation system cannot
    # handle the original string properly, as it is split in too many parts
    def status_text_waiting_response_overdue(info_request, opts = {})
      status_text = 'Selon la loi, ' + public_body_link(info_request.public_body) + \
                    ' aurait dû répondre avant le ' + \
                    simple_date(info_request.date_response_required_by) + \
                    '. Cette demande a donc reçu un <strong>refus implicite</strong>. '
      is_owning_user = opts.fetch(:is_owning_user)
      if is_owning_user
        status_text += 'Vous pouvez '
        status_text += link_to 'saisir la CADA.', send_cada_appeal_path(info_request.url_title)
      end
      _(status_text)
    end
  end

  CensorRule.class_eval do
    # override default method to force all strings to be encoded the same way, otherwise
    # gsub crashes. This first appeared on request 44527
    def apply_to_text(text_to_censor)
      return nil if text_to_censor.nil?

      text_to_censor.gsub(
        to_replace(text_to_censor.encoding),
        replacement.dup.force_encoding(text_to_censor.encoding)
      )
    end
  end

  AlaveteliTextMasker.class_eval do
    # override the default method to move email censor rules out
    # to the added method below
    def default_text_masks
      [{ to_replace: %r{(Mobile|Mob)([\s/]*(Fax|Tel))*\s*:?[\s\d]*\d},
         replacement: "[#{_('mobile number')}]" },
       { to_replace: %r{https?://#{AlaveteliConfiguration.domain}/c/[^\s]+},
         replacement: "[#{_('{{site_name}} login link',
                            site_name: site_name)}]" }]
    end

    def hide_all_emails_masks
      [{ to_replace: MySociety::Validate.email_find_regexp,
         replacement: "[#{_('email address')}]" }]
    end

    def apply_text_masks(text, options = {})
      # override the upstream method to make it possible to skip censoring
      # all emails by default.
      masks = options[:masks] || []
      masks += default_text_masks
      masks += hide_all_emails_masks if options[:hide_all_emails]
      censor_rules = options[:censor_rules] || []

      text = masks.inject(text) do |memo, mask|
        memo.gsub(mask[:to_replace], mask[:replacement])
      end

      censor_rules.reduce(text) { |t, rule| rule.apply_to_text(t) }
    end
  end

  OutgoingMessage::Template::InitialRequest.class_eval do
    # adjust the request template to remove too many \n before signature
    def template_string(replacements)
      msg = salutation(replacements)
      msg += letter(replacements)
      msg += "\n\n"
      msg += signoff(replacements)
      msg + "\n\n"
    end
  end

  #######################################################################
  # customise xapian indexing for better search
  #######################################################################
  # acts_as_xapian is passed:
  # texts: the fields of the model that need to be indexed, with a weight:
  #        higher numbers mean more important information
  User.class_eval do
    acts_as_xapian texts: [[:name, 10], [:about_me, 100]],
                   values: [
                     [:created_at_numeric, 1, 'created_at', :number] # for sorting
                   ],
                   terms: [[:variety, 'V', 'variety']],
                   if: :indexed_by_search?
  end

  FoiAttachment.class_eval do
    def indexed_by_search?
      # exclude tiny docs, such as images from email signatures, etc
      # Keep other doc types as they can contain useful information even
      # in very small files
      if display_size[-1] == 'K' &&
         (display_size[0..-2].to_i <= 15 &&
         ['image/jpeg', 'image/png', 'image/gif'].include?(content_type)) ||
         # use a higher limit for bmp files
         (display_size[0..-2].to_i <= 50 &&
         ['image/bmp'] .include?(content_type))
        logger.info("Not indexing tiny image #{id}")
        return false
      end

      # exclude some file types which are probably never useful to expose
      # such as email headers, and contact details
      return false if [
        'application/x-empty',
        'message/delivery-status',
        'message/disposition-notification',
        'text/rfc822-headers',
        'text/vcard',
        'text/x-vcard'
      ].include?(content_type)

      # exclude mail attachment in emails, which is the "message",
      if main_body_part?
        logger.info("Not indexing main body part #{id}")
        return false
      end

      # exclude docs from embargoed and hidden requests,
      # and messages themselves, which can be individually hidden
      if incoming_message.prominence != 'normal' ||
         incoming_message.info_request.embargoed? ||
         incoming_message.info_request.prominence != 'normal'
        logger.info("Not indexing hidden attachment #{id}")
        return false
      end

      # exclude attachments in error messages as they should not be interesting
      return false if incoming_message.error_message?

      # exclude docs from autoreplies and error messages
      if incoming_message.has_tag?("message:accuse_reception")
        logger.info("Not indexing accuse de reception #{id}")
        return false
      end
      logger.info("Indexing #{id}")
      true
    end

    def filesize
      body.size
    end

    def attachment_body_for_search
      incoming_message.get_attachment_text_full
    end

    # index attachments for retrieval in the /documents section of the site
    acts_as_xapian texts: [
                     [:filename, 100],
                     [:attachment_body_for_search, 100]
                   ],
                   values: [
                     [:filesize, 9, 'filesize', :number]
                   ],
                   terms: [
                     [:content_type, 'T', 'filetype']
                   ],
                   if: :indexed_by_search?
  end

  InfoRequestEvent.class_eval do
    acts_as_xapian \
      texts: [
        [:search_text_main, 100],
        [:title, 100]
      ],
      values: [
        # for QueryParser range searches e.g. 01/01/2008..14/01/2008:
        [:created_at, 0, 'range_search', :date],
        # for sorting:
        [:created_at_numeric, 1, 'created_at', :number],
        # TODO: using :number for lack of :datetime support in Xapian values:
        [:described_at_numeric, 2, 'described_at', :number],
        [:request, 3, 'request_collapse', :string],
        [:request_title_collapse, 4, 'request_title_collapse', :string]
      ],
      terms: [
        [:calculated_state, 'S', 'status'],
        [:requested_by, 'B', 'requested_by'],
        [:requested_from, 'F', 'requested_from'],
        [:commented_by, 'C', 'commented_by'],
        [:request, 'R', 'request'],
        [:variety, 'V', 'variety'],
        [:latest_variety, 'K', 'latest_variety'],
        [:latest_status, 'L', 'latest_status'],
        [:waiting_classification, 'W', 'waiting_classification'],
        [:filetype, 'T', 'filetype'],
        [:tags, 'U', 'tag'],
        [:request_public_body_tags, 'X', 'request_public_body_tag']
      ],
      eager_load: [
        :outgoing_message,
        :comment,
        { info_request: [:user, :public_body, :censor_rules] }
      ],
      if: :indexed_by_search?
  end

  PublicBody.class_eval do
    # fetch the linked json dict from local file
    # and cache it in memory

    # load data from the CADA directory after it has been processed nightly
    # by our monitoring tool
    def self.linked_cada_directory
      Rails.cache.fetch('cada_directory', expires_in: 3.hours) do
        url = 'https://gitlab.com/madada-team/annuaire-cada/-/raw/main/cada_directory_dict.json?ref_type=heads&inline=false'
        JSON.parse(URI.parse(url).open.read, { allow_nan: true })
      end
    end

    def load_cada_data
      PublicBody.linked_cada_directory[id.to_s]
    end

    # load the mapping of public_body_id => wikidata_id from WD and cache it
    def self.wikidata_identifiers
      Rails.cache.fetch('wikidata_identifiers', expires_in: 24.hours) do
        endpoint = "https://query.wikidata.org/sparql"
        sparql = <<'SPARQL'.chop
        SELECT DISTINCT ?idmadada ?item WHERE {
          SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
          {
            SELECT DISTINCT ?item ?idmadada WHERE {
              ?item p:P11139 ?id1.
              ?id1 (ps:P11139) _:anyValueP11139.
              ?item wdt:P11139 ?idmadada
            }
          }
        }
SPARQL

        client = SPARQL::Client.new(endpoint,
                                    method: :get,
                                    # user agent guidelines: https://w.wiki/CX6
                                    headers: {
                                      'User-Agent' => 'MaDadaBot/0.1 (https://madada.fr) Ruby'
                                    })
        rows = client.query(sparql)
        rows.to_h { |e| [e.idmadada.to_s, e.item.to_s] }
      end
    end

    # return the url to the wikidata element with id_madada matching self.url_name
    def wikidata_url
      PublicBody.wikidata_identifiers[url_name.to_s]
    end

    def name_for_search
      # special-case this one as the stemmer seems to get confused by the combination
      # of uppercase and l' prefix
      return "ministère intérieur" if name == "Ministère de l'Intérieur"

      name.downcase
    end

    def weighted_name_for_category
      # define a weight for the name field based on the category of the public body
      # so that "more important" ones come up first in results.
      # but: if default weight is 1000, mairie paris comes up before uni paris
      # but if default is 7000, then uni comes up first again, why?
      # notes are weighted less for a lower value here
      # best results so far with 800 as default weight
      w = 800
      tafs = tag_array_for_search
      if tafs.include? "mairie"
        w = 1800
      elsif tafs.include? "epci"
        w = 1800
      elsif tafs.include? "ministere"
        w = 2000
      elsif tafs.include? "cg"
        w = 2000
      elsif tafs.include? "cr"
        w = 2000
      elsif tafs.include? "prefecture"
        w = 1800
      elsif tafs.include? "prefecture_region"
        w = 1800
      end
      w
    end

    acts_as_xapian texts: [
                     [:name_for_search, :weighted_name_for_category],
                     [:short_name, 500],
                     [:notes_as_string, 50]
                   ],
                   values: [
                     # used for specific sorting, has no impact on the default
                     # order is "sortBy is not specified
                     [:info_requests_count, 5, "nb_demandes", :number],
                     [:created_at_numeric, 1, "created_at", :number]
                   ],
                   terms: [
                     [:name_for_search, 'N', 'name'],
                     [:variety, 'V', "variety"],
                     [:tag_array_for_search, 'U', "tag"]
                   ],
                   eager_load: [:translations]
  end

  PublicBodyHelper.class_eval do
    # override the handling of defunct tag so it can specify a replacement body
    # in the tag, like `defunct:<url_name of new body>`
    def public_body_not_requestable_reasons(public_body)
      reasons = []

      if public_body.defunct?
        new_body_url_names = public_body.get_tag_values('defunct')
        if !new_body_url_names.empty?
          new_url = public_body_link(PublicBody.find_by_url_name(new_body_url_names.first))
          reasons.push(sanitize("Cette administration n'existe plus, il faut désormais envoyer " \
                                "vos demandes à cette autorité : " \
                                "#{new_url}", tags: ['a']))
        else
          reasons.push _('This authority no longer exists, so you cannot make a request to it.')
        end
      end

      if public_body.not_apply?
        reasons.push _('Freedom of Information law does not apply to this authority, so you ' \
                       'cannot make a request to it.')
      end

      unless public_body.has_request_email?
        # Make the authority appear requestable to encourage users to help find
        # the authority's email address
        msg = link_to _("Make a request to this authority"),
          new_request_to_body_path(url_name: public_body.url_name),
          class: "link_button_green"

        reasons.push(msg)
      end

      reasons.compact
    end
  end

  PublicBodyCategoryLink.class_eval do
    # override the default method to add a count of bodies for each tag
    def self.by_display_order
      headings_table = Arel::Table.new(:public_body_headings)
      links_table = Arel::Table.new(:public_body_category_links)

      PublicBodyCategoryLink.
        select(headings_table[:display_order], links_table[Arel.star]).
        select('count(public_bodies.*) as count_of_bodies').
        joins(:public_body_heading).
        merge(PublicBodyHeading.by_display_order).
        joins(public_body_category: :public_bodies).
        merge(PublicBody.is_requestable).
        group('has_tag_string_tags.name',
               'public_body_headings.display_order',
               'public_body_category_links.public_body_category_id',
               'public_body_category_links.public_body_heading_id',
               'public_body_category_links.category_display_order',
               'public_body_category_links.id').
        order(:category_display_order).
        preload(
          public_body_heading: :translations,
          public_body_category: :translations
        )
    end
  end

  AlaveteliPro::Embargo.class_eval do
    THREE_MONTHS = 91.days
    SIX_MONTHS = 182.days
    TWELVE_MONTHS = 364.days
    TWENTYFOUR_MONTHS = 728.days

    # create a new constant as we can't modify the frozen DURATIONS hash
    NEW_DURATIONS = {
      "3_months" => proc { THREE_MONTHS },
      "6_months" => proc { SIX_MONTHS },
      "12_months" => proc { TWELVE_MONTHS },
      "24_months" => proc { TWENTYFOUR_MONTHS }
    }.freeze

    def allowed_durations
      NEW_DURATIONS.keys
    end

    def duration_as_duration(duration = nil)
      duration ||= embargo_duration
      NEW_DURATIONS[duration].call
    end

    def self.twentyfour_months_from_now
      Time.zone.now.beginning_of_day + TWENTYFOUR_MONTHS
    end
  end

  AlaveteliPro::Embargo::TranslatedConstants.class_eval do
    def self.duration_labels
      {
        "3_months" => _("3 Months"),
        "6_months" => _("6 Months"),
        "12_months" => _("12 Months"),
        "24_months" => _("24 Mois")
      }.freeze
    end
  end

  AlaveteliPro::InfoRequestsHelper.class_eval do
    def embargo_options_from_date(start_date)
      AlaveteliPro::Embargo::TranslatedConstants.
          duration_labels.map do |value, label|
        # duration = AlaveteliPro::Embargo::NEW_DURATIONS[value].call
        duration = NEW_DURATIONS[value].call
        expiry_date = I18n.l(start_date + duration, format: '%d %B %Y')
        [label, value, 'data-expiry-date' => expiry_date]
      end
    end
  end
end
