namespace :madada_reminders do
  desc 'Send daily reminders to public bodies who have not replied a week from deadline'
  task remind_after_three_weeks: :environment do
    deadline = Date.today + 7.days

    InfoRequest.where(date_response_required_by: deadline.beginning_of_day..deadline.end_of_day)
               .where(incoming_messages_count: 0)
               .joins('INNER JOIN public_bodies ON public_bodies.id = info_requests.public_body_id')
               .where('public_bodies.info_requests_count < 5')
               .each do |ir|
      RequestMailer.remind_one_week_before_deadline(
        ir, ir.public_body.request_email
      ).deliver_now
    end
  end
end
