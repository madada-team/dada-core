namespace :email_filters do
  desc 'Run filters on incoming emails'
  task filter_new_emails: :environment do
    IncomingMessage.filter_new_emails
  end
end
