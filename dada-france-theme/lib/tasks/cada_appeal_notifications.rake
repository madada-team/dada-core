namespace :madada_appeal_notifications do
  desc 'Send notification emails to all appealed bodies when the user sends an appeal to the CADA'
  task notify_public_bodies: :environment do
    join_sql = <<-EOF.strip_heredoc.squish
    INNER JOIN has_tag_string_tags
    ON has_tag_string_tags.model_type = 'InfoRequest'
    AND has_tag_string_tags.model_id = info_requests.id
    AND has_tag_string_tags.name = 'appeal_notification_pending'
    EOF
    # the 200 limit is arbitrary, to avoid huge surges of outgoing emails
    # which might cause trouble with spam filters
    InfoRequest.joins(join_sql).limit(200).each do |ir|
      check_tag = ir.calculate_appeal_notification_email_tag
      if ir.tag_string.include?(check_tag)
        OutgoingMailer.notify_appeal(ir).deliver_now
        # FIXME: replace this with ir.remove_tag once it has been fixed
        ir.tag_string = ir.tag_string.sub(/\b#{check_tag}\b/, '')
      end
    end
  end
end
