# A custom controller to manage the /documents route and the documents behind it

Rails.configuration.to_prepare do
  # Provide information about documents that have been obtained via DADA requests
  # This is read-only for now, and trying to avoid database changes.
  class DocumentsController < ApplicationController
    def list
      # TEMP: hide this page while testing
      return if cannot? :admin, AlaveteliPro::Embargo

      @query = params[:query] || ""
      @sortby = nil

      # TODO: list all docs when query is not set
      # collapse_by_prefix can be one of the "values" under acts_as_xapian
      @xapian_docs = ActsAsXapian::Search.new([FoiAttachment],
                                              @query,
                                              limit: 25,
                                              collapse_by_prefix: 'filesize')

      @docs = @xapian_docs.results.map { |r| r[:model] }
      logger.info(@xapian_docs.results)

      render template: 'documents/list'
    end
  end
end
