describe 'verifying that search ' do
  it 'tests do indeed work' do
    # this test should always pass, if not there is something
    # wrong with the test setup.
    expect(true).to eq(true)
  end
end


RSpec.describe ActsAsXapian::Search do
  describe "Document search" do
    before do
      update_xapian_index
    end

    it 'limits search by document type' do
      # 2 docs with different content_type but same filename/content
    end

    it 'limits search by document producer category' do
      # 2 docs by a mairie and a ministere, should return only one
    end

    it 'never shows embargoed documents' do
      # search for text in embargoed attachment, should not be in results
    end
