(function($) {
    // TODO: move this to search_cada_opinions.js once it works

    // words to remove from the original text
    // All words here should be lowercase
    var deadWords = [
        "a",
        "à",
        "au",
        "aux",
        "c",
        "ce",
        "ces",
        "cet",
        "cette",
        "d",
        "dans",
        "de",
        "des",
        "du",
        "elle",
        "elles",
        "il",
        "ils",
        "je",
        "l",
        "en",
        "et",
        "la",
        "le",
        "les",
        "madame",
        "monsieur",
        "nous",
        "ou",
        "par",
        "que",
        "qui",
        "s",
        "si",
        "un",
        "une",
        "vous",
    ];

    const initialTemplate = $("#outgoing_message_body").val();
    const [beforeTemplate, afterTemplate] = initialTemplate.split("-\n-\n");
    let lastQueryTS = Date.now();
    const queryInterval = 2000; // milliseconds
    let lastQueryText = "";

    /*
     * Trim the initial template content and deadwords
     * from the query text to help focus search on "useful" content.
     */
    function cleanQueryText(query) {
        noTemplate = query.replace(beforeTemplate, "").replace(afterTemplate, "");
        oneLiner = noTemplate
            .replaceAll("\n", " ")
            .replaceAll(/[-_,.()'’/?!]/g, " ")
            .toLowerCase();
        var out;
        out = deadWords.reduce(function(text, r) {
            regex = new RegExp(`\\b${r}\\b`, "img");
            return text.replaceAll(regex, " ");
        }, oneLiner);
        return out;
    }

    function getMeiliResults(queryText) {
        console.log("queryText", queryText);
        const postData = JSON.stringify({
            q: queryText,
            limit: 4,
            showRankingScore: true,
            showRankingScoreDetails: true,
            attributesToHighlight: ["Avis", "Objet"],
        });

        const searchHostname = "http://localhost:7700";
        // get this ansible vars for deployments
        const apiToken = "";
        const url = `${searchHostname}/indexes/avis_cada/search`;
        const settings = {
            data: postData,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
        };
        if (apiToken !== "") {
            settings.headers["Authorization"] = `Bearer ${apiToken}`;
        }

        $.ajax(url, settings).done(function(data) {
            const hits = data.hits;
            ul = $("#cada_reference ul");
            ul.empty();
            hits.forEach(function(hit) {
                const o =
                    hit._formatted.Objet === "[Voir avis]"
                        ? hit._formatted.Avis
                        : hit._formatted.Objet;
                const startIdx = o.search(/\s\S+\s<em>/gi);
                const excerpt = o.substring(startIdx, startIdx + 500);
                const urlAvis = `https://cada.data.gouv.fr/${hit.id}/`;
                ul.append(
                    `<li><a href="${urlAvis}" target="_blank">...${excerpt}...</a></li>`,
                );
            });
            $("#cada_reference").css("display", "block");
        });
    }

    $("#outgoing_message_body").on("keyup", function() {
        // naive rate limiting to prevent flooding the server
        // with queries
        const now = Date.now();
        if (now - lastQueryTS < queryInterval) {
            return;
        }
        // arrows and similar keys trigger keyup, no need to query
        // the same text again
        const queryText = $(this).val();
        if (queryText === lastQueryText) {
            return;
        }
        lastQueryTS = now;
        lastQueryText = queryText;

        cleanText = cleanQueryText(queryText);
        results = getMeiliResults(cleanText);
    });
})(jQuery);
