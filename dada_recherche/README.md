# Moteur de recherche de Ma Dada

Ce dossier contient le code servant à aggreger diverses données dans le moteur de recherche de Ma Dada et de
l'observatoire.

Le moteur utilise [meilisearch](https://www.meilisearch.com/) pour servir diverses recherches:

- pour l'observatoire: recherches géographiques d'administrations
- recherche d'administrations par catégorie, nom, etc...
- servir les données des divers graphiques utilisés dans l'observatoire (pas vraiment une recherche, mais meilisearch
  permet de servir des documents JSON très facilement, donc il peut faire office de serveur API pour des données
  pseudo-statiques comme ici (mises à jour via les scripts de ce dossier à intervalle régulier)).

# Mode d'emploi

Pour recharger l'index à partir de madada et wikidata:

```bash
poetry run python main.py
```

# Indexation des avis CADA

Ces avis sont utilisés lors de la rédaction d'une demande comme suggestion de références qui puissent appuyer la
demande.
Le fichier `cada-2024-01-11.csv` (ou sa dernière mise à jour) doit être téléchargé depuis
https://www.data.gouv.fr/fr/datasets/avis-et-conseils-de-la-cada/ puis la command suivante permet de charger son contenu
dans meilisearch.

```
cat cada-2024-01-11.csv | tr -d "\n" | sed "s/^Numéro de dossier,/id,/" | curl -X POST -H 'Content-Type: text/csv' http://localhost:7700/indexes/avis_cada/documents --data-binary @-
```

Pour le serveur, lancer cette commande sur le serveur même, car nginx est configuré pour rejeter un upload aussi gros.

Une clé api en lecture seule est nécessaire pour rechercher les avis. Elle est enregistrée dans les variables ansible
sous `meilisearch_apikey_cada_reference` et générée via:

```bash
curl -X POST 'https://recherche.madada.fr/keys' \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer <meili_master_key>' \
  --data-binary '{
    "description": "Search avis cada",
    "actions": ["search"],
    "indexes": ["avis_cada"],
    "expiresAt": "2029-01-01T00:00:00Z"
  }'
```
