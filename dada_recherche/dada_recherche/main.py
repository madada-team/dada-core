import os
from io import StringIO
from urllib.parse import urlencode

import httpx
import meilisearch
import polars as pl
from meilisearch.index import Index as MSIndex

db_user = os.getenv("DB_USERNAME")
db_password = os.getenv("DB_PASSWORD")
db_name = os.getenv("DB_NAME")
meili_api_key = os.getenv("MEILI_API_KEY")

uri = f"postgresql://{db_user}:{db_password}@localhost:5432/{db_name}"


def load_madada_data(db_uri: str) -> pl.DataFrame:
    query = """
SELECT
    pb.id as idmadada,
    pbv.url_name,
    pbv.name,
    'invalid@example.com' as contact_prada,
    pb.info_requests_count as nb_requests,
    pb.info_requests_successful_count as nb_successful_requests,
    EXISTS(
        SELECT name
        FROM has_tag_string_tags tags
        WHERE model_id=pb.id AND model_type='PublicBody'
        AND name='rip'
    ) as rip,
    EXISTS(
        SELECT name, value
        FROM has_tag_string_tags tags
        WHERE model_id=pb.id AND model_type='PublicBody'
        AND name='prada' AND value='obligatoire'
    ) as prada_req,
    EXISTS(
        SELECT name, value
        FROM has_tag_string_tags tags
        WHERE model_id=pb.id AND model_type='PublicBody'
        AND name='prada' AND value='nommee'
    ) as prada_named,
    -- TODO: calculate these
    RANDOM() * 100 as pc_not_replied,
    RANDOM() * 100 as pc_cada_followed,
    RANDOM() * 100 as pc_cada_decisions_without_response,
    RANDOM() * 100 as taux_reponse_moyen,
    RANDOM() * 10 as nb_moyen_demandes,
    '2020-01-01' as year_ref,
    COALESCE((SELECT json_agg(json_build_object('url', lr.url_title, 'title', lr.title, 'date', lr.last_public_response_at))
        FROM (
            SELECT url_title, title, last_public_response_at
            FROM info_requests ir
            WHERE ir.public_body_id=pb.id
            AND (ir.described_state = 'successful' OR ir.described_state = 'partially_successful')
            LIMIT 3) lr
    ), '[]'::json) AS last_requests,
    cat.category_tag as category
FROM
    public_bodies pb
JOIN public_body_versions pbv
    ON pb.id = pbv.public_body_id AND pb.version = pbv.version
JOIN has_tag_string_tags tags
    ON tags.model_type = 'PublicBody'
    AND tags.model_id = pb.id
JOIN public_body_categories cat
    ON cat.category_tag=tags.name
    """
    df = pl.read_database_uri(query=query, uri=db_uri)
    # convert booleans from postgres format
    for c in ["rip", "prada_req", "prada_named"]:
        df = df.with_columns((pl.col(c) == "t").alias(c))
    return df


def load_wikidata_data() -> pl.DataFrame:
    endpoint_url = "https://query.wikidata.org/sparql"

    # this gets only city halls
    # TODO: get only first post code
    query: str = """SELECT DISTINCT ?item ?itemLabel ?adresse ?cp ?lng ?lat ?idmadada ?pop ?url_logo WHERE {
      SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
      {
        SELECT DISTINCT ?item ?ressort_territorial ?adresse (MIN(?code_postal) as ?cp) ?lng ?lat ?idmadada ?pop ?url_logo WHERE {
          ?item p:P11139 ?id1.
          ?id1 ps:P11139 _:anyValueP11139.
          ?item wdt:P11139 ?idmadada;
            wdt:P31 wd:Q86442081;
            wdt:P1001 ?ressort_territorial.
          ?ressort_territorial wdt:P1082 ?pop.
          ?ressort_territorial wdt:P281 ?code_postal.
          ?item wdt:P6375 ?adresse.
          ?ressort_territorial wdt:P625 ?coords.
          BIND(xsd:decimal(geof:longitude(?coords)) AS ?lng)
          BIND(xsd:decimal(geof:latitude(?coords)) AS ?lat)
          OPTIONAL {?ressort_territorial wdt:P154 ?image.}
          BIND(REPLACE(wikibase:decodeUri(STR(?image)), "http://commons.wikimedia.org/wiki/Special:FilePath/", "") as ?fileName) .
          BIND(REPLACE(?fileName, " ", "_") as ?safeFileName)
          BIND(MD5(?safeFileName) as ?fileNameMD5) .
          BIND(CONCAT("https://upload.wikimedia.org/wikipedia/commons/", SUBSTR(?fileNameMD5, 1, 1), "/", SUBSTR(?fileNameMD5, 1, 2), "/", ?safeFileName) as ?url_logo)
        }
        GROUP BY ?item ?ressort_territorial ?adresse ?lng ?lat ?idmadada ?pop ?url_logo
      }
    }"""

    # TODO: add queries for other types of entities
    args = urlencode([("query", query)])
    full_query = f"{endpoint_url}?{args}"
    # see user agent guidelines at https://w.wiki/CX6
    user_agent = "MaDadaBot/0.1 (https://madada.fr) python/3.11"
    headers = {
        "user-agent": user_agent,
        "Accept": "text/csv",
    }
    results = httpx.get(full_query, headers=headers, timeout=30.0)

    return pl.read_csv(StringIO(results.text))


def join_dfs(dada_df: pl.DataFrame, wd_df: pl.DataFrame) -> pl.DataFrame:
    dtype = pl.List(
        pl.Struct(
            [
                pl.Field("url", pl.Utf8),
                pl.Field("title", pl.Utf8),
                pl.Field("date", pl.Utf8),
            ]
        )
    )

    df = dada_df.join(wd_df, left_on="url_name", right_on="idmadada", how="inner").drop(
        ["coords", "itemLabel", "item"]
    )
    df = df.with_columns(last_requests=pl.col("last_requests").str.json_decode(dtype))
    return df


def get_or_create_meilisearch_index(index_name: str) -> MSIndex:
    """If the index already exists, the call will return a task with failed status"""
    client = meilisearch.Client("http://localhost:7700", meili_api_key)
    client.create_index(index_name, {"primaryKey": "url_name"})
    idx = client.index(index_name)
    return idx


def upload_to_meilisearch(df: pl.DataFrame) -> None:
    """
    Upload the data to meilisearch as JSON, as we need nested structures
    (geo and last requests).
    """
    index_name = "autorites"
    idx = get_or_create_meilisearch_index(index_name)
    docs_task_id = idx.add_documents(df.to_dicts()).task_uid
    # wait for the docs to be indexed, this means the service will have a short
    # downtime of a few seconds at night
    idx.wait_for_task(docs_task_id, timeout_in_ms=30_000)
    # make sure the _geo field is filterable
    idx.update_filterable_attributes(["_geo", "category"])
    # define which fields are searchable, and their relative weight
    idx.update_searchable_attributes(["name", "url_name", "cp", "categorie"])
    idx.update_sortable_attributes(["name", "nb_requests", "nb_successful_requests"])
    idx.update_settings({"pagination": {"maxTotalHits": 60000}})


def main() -> None:
    dada_df = load_madada_data(uri)
    wd_df = load_wikidata_data()
    wd_df = wd_df.with_columns(pl.col("pop").cast(pl.Int32))
    wd_df = wd_df.with_columns(_geo=wd_df.select(["lng", "lat"]).to_struct("_geo"))
    full_df = join_dfs(dada_df, wd_df)
    upload_to_meilisearch(full_df)


if __name__ == "__main__":
    main()
